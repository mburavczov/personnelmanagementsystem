export function formatDate(date?: string | Date | null) {
  if (!date) return undefined;

  return new Date(date).toLocaleDateString('ru', {
    month: 'short',
    day: 'numeric',
    year: 'numeric'
  });
}
