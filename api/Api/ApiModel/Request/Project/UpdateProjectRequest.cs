﻿namespace Api.ApiModel.Request.Project
{
    public class UpdateProjectRequest
    {
        public int ProjectId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ManagerId { get; set; }
        public int CustomerId { get; set; }
    }
}