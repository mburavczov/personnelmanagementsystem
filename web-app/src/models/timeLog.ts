import { User } from '@/models/user';

export interface TimeLog {
  id: number;
  person: User;
  minutes: number;
  comment: string;
  createdAt: Date;
}
