﻿using System.Collections.Generic;
using Api.ApiModel.Response.Profile;
using Api.ApiModel.Response.ProjectTask;

namespace Api.ApiModel.Response.Project
{
    public class ProjectItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public PersonItem Manager { get; set; }
        public PersonItem Customer { get; set; }
        public List<PersonItem> Persons { get; set; }
        public List<TaskItem> Tasks { get; set; }
    }
}