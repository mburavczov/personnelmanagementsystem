import {
  alpha,
  Box,
  Card,
  Divider,
  LinearProgress,
  linearProgressClasses,
  Stack,
  styled,
  Typography,
  useTheme
} from '@mui/material';
import React from 'react';
import { useRouterQueryId } from '@/hooks';
import { useProjectDetailQuery } from '@/api';
import { TaskStatuses } from '@/models/enums';
import { taskStatusesMapping } from '@/utils';

export function TasksProjects() {
  const theme = useTheme();
  const projectId = useRouterQueryId();
  const { data: project } = useProjectDetailQuery({ projectId });

  if (!project) {
    return null;
  }

  const progressItems = [
    {
      title: 'Новые',
      desc: 'Кол-во новых задач',
      color: taskStatusesMapping[TaskStatuses.NEW].color,
      count: project.tasks.filter(({ status }) => status === TaskStatuses.NEW)
        .length
    },
    {
      title: 'В процессе',
      desc: 'Кол-во задач в процессе',
      color: taskStatusesMapping[TaskStatuses.IN_PROGRESS].color,
      count: project.tasks.filter(
        ({ status }) => status === TaskStatuses.IN_PROGRESS
      ).length
    },
    {
      title: 'В ожидании',
      desc: 'Кол-во задач в ожидании',
      color: taskStatusesMapping[TaskStatuses.WAIT].color,
      count: project.tasks.filter(({ status }) => status === TaskStatuses.WAIT)
        .length
    },
    {
      title: 'Выполенные',
      desc: 'Кол-во выполненных задач',
      color: taskStatusesMapping[TaskStatuses.DONE].color,
      count: project.tasks.filter(({ status }) => status === TaskStatuses.DONE)
        .length
    }
  ];

  const countTasks = project.tasks.length;

  return (
    <Card>
      <Stack
        direction={{ xs: 'column', lg: 'row' }}
        divider={<Divider orientation="vertical" flexItem />}
        justifyContent="space-between"
        alignItems="stretch"
        spacing={0}
      >
        {progressItems.map((item) => (
          <Box p={2.5} flexGrow={1} key={item.color}>
            <Box
              mb={2}
              display="flex"
              alignItems="center"
              justifyContent="space-between"
            >
              <Box>
                <Typography color="text.primary" variant="h4" gutterBottom>
                  {item.title}
                </Typography>
                <Typography variant="subtitle2" noWrap>
                  {item.desc}
                </Typography>
              </Box>
              <Typography
                variant="h2"
                sx={{
                  color: `${theme.colors[item.color].main}`
                }}
              >
                {item.count}
              </Typography>
            </Box>
            <MyLinearProgress
              baseColor={item.color}
              variant="determinate"
              value={(item.count * 100) / countTasks}
            />
            <Box
              display="flex"
              sx={{
                mt: 0.6
              }}
              alignItems="center"
              justifyContent="space-between"
            >
              <Typography
                sx={{
                  color: `${theme.colors.alpha.black[50]}`
                }}
                variant="subtitle2"
              >
                0%
              </Typography>
              <Typography
                sx={{
                  color: `${theme.colors.alpha.black[50]}`
                }}
                variant="subtitle2"
              >
                100%
              </Typography>
            </Box>
          </Box>
        ))}
      </Stack>
    </Card>
  );
}

const MyLinearProgress = styled(LinearProgress)<{ baseColor: string }>(
  ({ theme, baseColor }) => `
        height: 8px;
        border-radius: ${theme.general.borderRadiusLg};

        &.${linearProgressClasses.colorPrimary} {
            background-color: ${alpha(theme.colors[baseColor].main, 0.1)};
        }
        
        & .${linearProgressClasses.bar} {
            border-radius: ${theme.general.borderRadiusLg};
            background-color: ${theme.colors[baseColor].main};
        }
    `
);
