﻿namespace Api.ApiModel.Request.Project
{
    public class CreateProjectRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int ManagerId { get; set; }
        public int CustomerId { get; set; }
    }
}