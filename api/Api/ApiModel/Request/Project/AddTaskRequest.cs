﻿namespace Api.ApiModel.Request.Project
{
    public class AddTaskRequest
    {
        public int ProjectId { get; set; }
        public string TaskName { get; set; }
        public string TaskDescription { get; set; }
        public int? AssignedId { get; set; }
    }
}