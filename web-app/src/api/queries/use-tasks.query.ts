import { useQuery } from 'react-query';
import { client } from '@/api';
import { Task } from '@/models';

export type GetTasksResponse = {
  tasks: Task[];
};

export function useTasksQuery() {
  return useQuery<GetTasksResponse>(
    ['tasks'],
    async () => {
      try {
        const dto = await client.get('ProjectTask/GetTask');
        return dto.data;
      } catch {
        console.log('fatal error');
      }
    },
    {}
  );
}
