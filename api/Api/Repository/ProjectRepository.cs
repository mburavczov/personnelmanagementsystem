﻿using System.Collections.Generic;
using System.Linq;
using Api.Entity;
using Microsoft.EntityFrameworkCore;

namespace Api.Repository
{
    public class ProjectRepository : BaseRepository<Project>
    {
        public ProjectRepository(MainContext context) : base(context)
        {
        }
        
        public List<Project> GetAll()
        {
            return DbSet
                .Include(x => x.ProjectTasks)
                .Include(x => x.ProjectTasks).ThenInclude(x => x.Assigned)
                .Include(x => x.ProjectTasks).ThenInclude(x => x.TimeLogs)
                .Include(x => x.Persons)
                .ToList();
        }

        public Project GetById(int id)
        {
            return DbSet
                .Include(x => x.Persons)
                .Include(x => x.ProjectTasks)
                .Include(x => x.ProjectTasks).ThenInclude(x => x.Assigned)
                .Include(x => x.ProjectTasks).ThenInclude(x => x.TimeLogs)
                .FirstOrDefault(x => x.Id == id);
        }

        public List<Project> GetByPersonId(int personId)
        {
            return DbSet
                .Include(x => x.Persons)
                .Include(x => x.ProjectTasks)
                .Include(x => x.ProjectTasks).ThenInclude(x => x.Assigned)
                .Include(x => x.ProjectTasks).ThenInclude(x => x.TimeLogs)
                .Where(x => x.Persons.Any(p => p.Id == personId))
                .ToList();
        }
    }
}