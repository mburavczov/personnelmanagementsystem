import { useRef, useState } from 'react';
import { useAuth } from 'src/hooks/useAuth';
import { useRouter } from 'next/router';

import {
  Avatar,
  Box,
  Button,
  Divider,
  MenuList,
  alpha,
  IconButton,
  MenuItem,
  ListItemText,
  Popover,
  Typography,
  styled,
  useTheme
} from '@mui/material';
import LockOpenTwoToneIcon from '@mui/icons-material/LockOpenTwoTone';
import ChevronRightTwoToneIcon from '@mui/icons-material/ChevronRightTwoTone';
import { Link } from '@/components';

export function HeaderUserbox() {
  const theme = useTheme();
  const router = useRouter();

  const { logout, user } = useAuth();

  const ref = useRef<any>(null);
  const [isOpen, setOpen] = useState<boolean>(false);

  const handleOpen = (): void => {
    setOpen(true);
  };

  const handleClose = (): void => {
    setOpen(false);
  };

  const handleLogout = async (): Promise<void> => {
    try {
      handleClose();
      await logout();
      router.push('/');
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <>
      <UserBoxButton color="primary" ref={ref} onClick={handleOpen}>
        <Avatar
          alt={user?.name}
          src={user?.avatarPath}
          sx={{
            width: '90%',
            height: '90%',
            borderRadius: theme.general.borderRadiusLg
          }}
        />
      </UserBoxButton>
      <Popover
        disableScrollLock
        anchorEl={ref.current}
        onClose={handleClose}
        open={isOpen}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
      >
        <Box
          sx={{
            p: 2,
            minWidth: 300,
            background: alpha(theme.colors.alpha.black[100], 0.08)
          }}
          display="flex"
        >
          <Avatar variant="rounded" alt={user?.name} src={user?.avatarPath} />
          <Box sx={{ pl: 1 }}>
            <Typography
              variant="body1"
              sx={{
                color: theme.palette.secondary.main,
                fontWeight: theme.typography.fontWeightBold
              }}
            >
              {user?.name} {user?.surname}
            </Typography>
            <Typography
              variant="body2"
              sx={{
                color: theme.palette.secondary.light
              }}
            >
              {user?.position}
            </Typography>
          </Box>
        </Box>
        <Divider
          sx={{
            mb: 0
          }}
        />
        <MenuListWrapperPrimary disablePadding sx={{ pt: 2 }}>
          <MenuItem component={Link} href="/profile" onClick={handleClose}>
            <ListItemText
              primaryTypographyProps={{
                variant: 'h5'
              }}
              primary="Мой профиль"
            />
            <ChevronRightTwoToneIcon
              sx={{
                color: `${theme.colors.alpha.black[30]}`,
                opacity: 0.8
              }}
            />
          </MenuItem>
        </MenuListWrapperPrimary>
        <MenuListWrapperPrimary disablePadding sx={{ pt: 1 }}>
          <MenuItem component={Link} href="/projects" onClick={handleClose}>
            <ListItemText
              primaryTypographyProps={{
                variant: 'h5'
              }}
              primary="Проекты"
            />
            <ChevronRightTwoToneIcon
              sx={{
                color: `${theme.colors.alpha.black[30]}`,
                opacity: 0.8
              }}
            />
          </MenuItem>
        </MenuListWrapperPrimary>
        <MenuListWrapperPrimary disablePadding sx={{ pt: 1, pb: 2 }}>
          <MenuItem component={Link} href="/tasks" onClick={handleClose}>
            <ListItemText
              primaryTypographyProps={{
                variant: 'h5'
              }}
              primary="Задачи"
            />
            <ChevronRightTwoToneIcon
              sx={{
                color: `${theme.colors.alpha.black[30]}`,
                opacity: 0.8
              }}
            />
          </MenuItem>
        </MenuListWrapperPrimary>
        <Divider />
        <Box m={1}>
          <Button color="primary" fullWidth onClick={handleLogout}>
            <LockOpenTwoToneIcon
              sx={{
                mr: 1
              }}
            />
            Выйти
          </Button>
        </Box>
      </Popover>
    </>
  );
}

const UserBoxButton = styled(IconButton)(
  ({ theme }) => `
  width: ${theme.spacing(4)};
  padding: 0;
  height: ${theme.spacing(4)};
  margin-left: ${theme.spacing(1)};
  border-radius: ${theme.general.borderRadiusLg};
  
  &:hover {
    background: ${theme.colors.primary.main};
  }
`
);

const MenuListWrapperPrimary = styled(MenuList)(
  ({ theme }) => `
  padding-left: ${theme.spacing(2)};
  padding-right: ${theme.spacing(2)};

  & .MuiMenuItem-root {
    border-radius: 50px;
    padding: ${theme.spacing(1, 1, 1, 2.5)};
    min-width: 200px;
    margin-bottom: 2px;
    position: relative;
    color: ${theme.colors.alpha.black[100]};

    &.Mui-selected,
    &:hover,
    &.MuiButtonBase-root:active {
        background: ${theme.colors.primary.lighter};
        color: ${theme.colors.primary.main};
    }

    &:last-child {
        margin-bottom: 0;
    }
  }
`
);
