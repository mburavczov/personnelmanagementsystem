﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Api.Settings;
using Microsoft.Extensions.Options;

namespace Api.Services
{
    public class CdnService
    {
        private readonly CdnSettings _cdnSettings;

        public CdnService(IOptionsSnapshot<CdnSettings> cdnSettings)
        {
            _cdnSettings = cdnSettings.Value;
        }

        public async Task Upload(byte[] data, string path)
        {
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Add("X-Auth-Token", Environment.GetEnvironmentVariable("CDN_TOKEN"));
                var res = await httpClient.PutAsync($"{_cdnSettings.Url}/avatars/{path}", new ByteArrayContent(data));
                res.EnsureSuccessStatusCode();
            }
        }
    }
}