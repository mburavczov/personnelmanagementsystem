import { ReactNode } from 'react';
import { Box, Breakpoint, Container, styled } from '@mui/material';

interface CenterContentProps {
  children: ReactNode;
  maxWidth?: Breakpoint;
}

export function CenterContent({
  children,
  maxWidth = 'sm'
}: CenterContentProps) {
  return (
    <MainContent>
      <TopWrapper>
        <Container maxWidth={maxWidth}>{children}</Container>
      </TopWrapper>
    </MainContent>
  );
}

const MainContent = styled(Box)`
  height: 100%;
  display: flex;
  flex: 1;
  flex-direction: column;
`;

const TopWrapper = styled(Box)`
  display: flex;
  width: 100%;
  flex: 1;
  align-items: center;
  justify-content: center;
`;
