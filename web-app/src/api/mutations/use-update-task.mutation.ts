import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';
import { TaskStatuses } from '@/models/enums';

export interface UpdateTaskRequest {
  id: number;
  name: string;
  description: string;
  assignedId: number;
  status: TaskStatuses;
}

export function useUpdateTaskMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, UpdateTaskRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, UpdateTaskRequest>(
    (data) => client.post('ProjectTask/Update', data),
    options
  );

  return mutation.mutateAsync;
}
