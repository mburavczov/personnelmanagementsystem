import { Box, Card, styled, Typography, useTheme } from '@mui/material';
import { useRouterQueryId } from '@/hooks';
import { useProjectDetailQuery } from '@/api';
import { taskStatusesMapping } from '@/utils';
import { TaskStatuses } from '@/models/enums';
import React from 'react';
import { TaskCard } from '@/modules/projects/task-detail/TaskCard';

export function ProjectsBoard() {
  const theme = useTheme();
  const projectId = useRouterQueryId();
  const { data: project } = useProjectDetailQuery({ projectId });

  if (!project) {
    return null;
  }

  const { tasks } = project;

  const tasksByStatus = [
    {
      title: 'Новые',
      color: taskStatusesMapping[TaskStatuses.NEW].color,
      tasks: tasks.filter(({ status }) => status === TaskStatuses.NEW)
    },
    {
      title: 'В процессе',
      color: taskStatusesMapping[TaskStatuses.IN_PROGRESS].color,
      tasks: tasks.filter(({ status }) => status === TaskStatuses.IN_PROGRESS)
    },
    {
      title: 'В ожидании',
      color: taskStatusesMapping[TaskStatuses.WAIT].color,
      tasks: tasks.filter(({ status }) => status === TaskStatuses.WAIT)
    },
    {
      title: 'Выполенные',
      color: taskStatusesMapping[TaskStatuses.DONE].color,
      tasks: tasks.filter(({ status }) => status === TaskStatuses.DONE)
    }
  ];

  return (
    <TasksWrapper>
      {tasksByStatus.map(({ title, color, tasks }) => {
        return (
          <ListColumnWrapper
            sx={{
              borderColor: theme.colors[color].main
            }}
          >
            <Box
              px={2}
              pt={2}
              display="flex"
              justifyContent="space-between"
              alignItems="center"
            >
              <Typography color="inherit" variant="h3">
                {title}
              </Typography>
            </Box>
            {tasks.length === 0 && (
              <Box p={4} textAlign="center">
                <Typography variant="subtitle2">
                  Нет задач с таким статусом
                </Typography>
              </Box>
            )}
            {tasks.map((task) => (
              <TaskCard task={task} projectId={projectId} key={task.id} />
            ))}
          </ListColumnWrapper>
        );
      })}
    </TasksWrapper>
  );
}

const TasksWrapper = styled(Box)(
  ({ theme }) => `
      display: flex;
      overflow-y: hidden;
      overflow-x: auto;
      flex-direction: row;
      padding: ${theme.spacing(1)};
  `
);

const ListColumnWrapper = styled(Card)(
  ({ theme }) => `
      width: 340px;
      min-width: 280px;
      margin-right: ${theme.spacing(3)};
      border-top-width: 8px;
      border-top-style: solid;
  `
);
