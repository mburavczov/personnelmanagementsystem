﻿using System.Linq;
using Api.ApiModel.Request.Project;
using Api.ApiModel.Request.ProjectTask;
using Api.ApiModel.Response.ProjectTask;
using Api.Entity;
using Api.Helper;
using Api.Repository;
using Api.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// Methods for manage project tasks
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectTaskController : Controller
    {
        private readonly ProjectTaskRepository _projectTaskRepository;
        private readonly TimeLogRepository _timeLogRepository;

        public ProjectTaskController(ProjectTaskRepository projectTaskRepository, TimeLogRepository timeLogRepository)
        {
            _projectTaskRepository = projectTaskRepository;
            _timeLogRepository = timeLogRepository;
        }

        /// <summary>
        /// Get person tasks
        /// </summary>
        /// <response code="200">Ok</response>
        [Authorize]
        [HttpGet("GetTasks")]
        [ProducesResponseType(typeof(GetTasksResponse), 200)]
        public IActionResult GetTasks(int? personId)
        {
            if (!personId.HasValue)
            {
                personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            }

            var tasks = _projectTaskRepository.GetByPersonId(personId.Value);
            return Ok(new GetTasksResponse { Tasks = tasks.Select(TaskHelper.MapProjectTask).ToList() });
        }

        /// <summary>
        /// Get person tasks
        /// </summary>
        /// <response code="200">Ok</response>
        [Authorize]
        [HttpGet("GetTask")]
        [ProducesResponseType(typeof(TaskItem), 200)]
        public IActionResult GetTask(int taskId)
        {
            var task = _projectTaskRepository.GetById(taskId);
            return Ok(TaskHelper.MapProjectTask(task));
        }
        
        /// <summary>
        /// Update project task
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="400">Task closed and can't be updated</response>
        /// <response code="404">Task not found</response>
        [Authorize(Roles = "admin,manager,employee")]
        [HttpPost("Update")]
        public IActionResult Update(UpdateRequest data)
        {
            var task = _projectTaskRepository.GetById(data.Id);
            if (task == null)
            {
                return NotFound();
            }

            if (task.Status == ProjectTaskStatus.Done)
            {
                return BadRequest();
            }

            task.Name = data.Name;
            task.Description = data.Description;
            task.Status = data.Status;
            task.AssignedId = data.AssignedId;
            
            _projectTaskRepository.Update(task);

            return Ok();
        }

        /// <summary>
        /// Add time log
        /// </summary>
        /// <response code="200">Ok</response>
        [Authorize(Roles = "admin,manager,employee")]
        [HttpPost("AddTimeLog")]
        public IActionResult AddTimeLog(AddTimeLogRequest data)
        {
            _timeLogRepository.Add(new TimeLog
            {
                PersonId = data.PersonId,
                TaskId = data.TaskId,
                Minutes = data.Minutes,
                Comment = data.Comment,
            });

            return Ok();
        }
    }
}