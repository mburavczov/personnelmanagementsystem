import { Box, Divider, styled } from '@mui/material';
import { Scrollbar } from '@/components';
import { useContext } from 'react';
import { ChatContext } from '@/contexts/ChatContext';
import { SidebarContent } from '@/modules/chat/components/SidebarContent';
import { ChatContent } from '@/modules/chat/components/ChatContent';
import { BottomBarContent } from '@/modules/chat/components/BottomBarContent';

export function ChatPage() {
  const { chatId, setSelectUser } = useContext(ChatContext);

  return (
    <RootWrapper className="Mui-FixedWrapper">
      <Sidebar>
        <Scrollbar>
          <SidebarContent changeSelect={(s) => setSelectUser(s)} />
        </Scrollbar>
      </Sidebar>
      <ChatWindow>
        <Box flex={1}>
          <Scrollbar>
            <ChatContent />
          </Scrollbar>
        </Box>
        <Divider />
        {chatId ? <BottomBarContent /> : null}
      </ChatWindow>
    </RootWrapper>
  );
}

const RootWrapper = styled(Box)(
  ({ theme }) => `
       height: calc(100vh - ${theme.header.height});
       display: flex;
`
);

const Sidebar = styled(Box)(
  ({ theme }) => `
        width: 300px;
        background: ${theme.colors.alpha.white[100]};
        border-right: ${theme.colors.alpha.black[10]} solid 1px;
`
);

const ChatWindow = styled(Box)(
  () => `
        width: 100%;
        height: 100%;
        display: flex;
        flex-direction: column;
        flex: 1;
`
);
