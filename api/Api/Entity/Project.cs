﻿using System.Collections.Generic;
using System.Linq;

namespace Api.Entity
{
    public class Project
    {
        public Project()
        {
            Persons = new List<Person>();
            ProjectTasks = new List<ProjectTask>();
        }
        
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ManagerId { get; set; }
        public int CustomerId { get; set; }

        public virtual Person Manager => Persons?.FirstOrDefault(x => x.Id == ManagerId);
        public virtual Person Customer => Persons?.FirstOrDefault(x => x.Id == CustomerId);
        public virtual ICollection<Person> Persons { get; set; }
        public virtual ICollection<ProjectTask> ProjectTasks { get; set; }
    }
}