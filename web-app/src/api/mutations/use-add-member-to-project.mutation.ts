import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

export interface AddMemberToProjectRequest {
  personId: number;
  projectId: number;
}

export function useAddMemberToProjectMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, AddMemberToProjectRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, AddMemberToProjectRequest>(
    (data) => client.post('Project/AddMember', data),
    options
  );

  return mutation.mutateAsync;
}
