import { User } from '@/models/user';
import { Task } from '@/models/task';

export interface Project {
  id: number;
  name: string;
  description: string;
  manager: User;
  customer: User;
  persons: User[];
  tasks: Task[];
}
