import { TaskStatuses } from '@/models/enums';

export const taskStatusesMapping = {
  [TaskStatuses.WAIT]: {
    color: 'warning',
    label: 'В ожидании',
    tooltip: 'Задача ждет какого-либо действия'
  },
  [TaskStatuses.NEW]: {
    color: 'info',
    label: 'Новая',
    tooltip: 'Новая задача'
  },
  [TaskStatuses.IN_PROGRESS]: {
    color: 'error',
    label: 'В процессе',
    tooltip: 'Задача в процессе выполнения'
  },
  [TaskStatuses.DONE]: {
    color: 'success',
    label: 'Готово',
    tooltip: 'Задача выполнена'
  }
};
