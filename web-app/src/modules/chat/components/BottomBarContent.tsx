import {
  Avatar,
  Box,
  Button,
  styled,
  InputBase,
  useTheme
} from '@mui/material';
import SendTwoToneIcon from '@mui/icons-material/SendTwoTone';
import { useSendMessageMutation } from '@/api';
import { useContext, useEffect, useState } from 'react';
import { ChatContext } from '@/contexts/ChatContext';
import { useAuth } from '@/hooks';
import { useQueryClient } from 'react-query';

export function BottomBarContent() {
  const queryClient = useQueryClient();
  const { user } = useAuth();
  const theme = useTheme();
  const [message, setMessage] = useState('');
  const { chatId } = useContext(ChatContext);
  const sendMessage = useSendMessageMutation({
    onSuccess: () => {
      queryClient.invalidateQueries(['chatMessages', chatId]);
      setMessage('');
    }
  });

  useEffect(() => {
    setMessage('');
  }, [chatId]);

  return (
    <Box
      sx={{
        background: theme.colors.alpha.white[50],
        display: 'flex',
        alignItems: 'center',
        p: 2
      }}
    >
      <Box flexGrow={1} display="flex" alignItems="center">
        <Avatar
          sx={{ display: { xs: 'none', sm: 'flex' }, mr: 1 }}
          src={user?.avatarPath}
        />
        <MessageInputWrapper
          autoFocus
          placeholder="Наберите Ваше сообщение..."
          fullWidth
          value={message}
          onChange={(e) => setMessage(e.target.value)}
          onKeyPress={(event) => {
            if (event.key === 'Enter') {
              sendMessage({
                chatId: chatId,
                text: message
              });
            }
          }}
        />
      </Box>
      <Box>
        <Button
          onClick={() =>
            sendMessage({
              chatId: chatId,
              text: message
            })
          }
          startIcon={<SendTwoToneIcon />}
          variant="contained"
        >
          Отправить
        </Button>
      </Box>
    </Box>
  );
}

const MessageInputWrapper = styled(InputBase)(
  ({ theme }) => `
    font-size: ${theme.typography.pxToRem(18)};
    padding: ${theme.spacing(1)};
    width: 100%;
`
);
