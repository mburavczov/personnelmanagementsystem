﻿namespace Api.ApiModel.Request.Account
{
    public class CreateAccountRequest
    {
        public string Login { get; set; }
        public string Pass { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string About { get; set; }
        public string Position { get; set; }
    }
}