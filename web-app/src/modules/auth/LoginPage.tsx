import { CenterContent, Logo } from '@/components';
import { Box, Typography, Card } from '@mui/material';
import { LoginForm } from '@/modules/auth/LoginForm';

export function LoginPage() {
  return (
    <CenterContent>
      <Box textAlign="center">
        <Logo />
      </Box>
      <Card
        sx={{
          p: 4,
          my: 4
        }}
      >
        <Box textAlign="center">
          <Typography
            variant="h2"
            sx={{
              mb: 1
            }}
          >
            Вход
          </Typography>
          <Typography
            variant="h4"
            color="text.secondary"
            fontWeight="normal"
            sx={{
              mb: 3
            }}
          >
            Заполните поля ниже, чтобы войти в свою учетную запись.
          </Typography>
        </Box>
        <LoginForm />
      </Card>
    </CenterContent>
  );
}
