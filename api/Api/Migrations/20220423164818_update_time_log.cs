﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Api.Migrations
{
    public partial class update_time_log : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Comment",
                table: "TimeLogs",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Comment",
                table: "TimeLogs");
        }
    }
}
