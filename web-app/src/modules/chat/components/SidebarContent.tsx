import {
  Box,
  Typography,
  Avatar,
  List,
  ListItemButton,
  ListItemAvatar,
  ListItemText,
  styled
} from '@mui/material';
import * as React from 'react';
import { useContext } from 'react';
import { ChatContext } from '@/contexts/ChatContext';
import { useAuth } from '@/hooks';
import { useChats, usePersonsQuery } from '@/api';

export function SidebarContent() {
  const { data } = useChats();
  const { data: users = [] } = usePersonsQuery({ role: undefined });
  const { user } = useAuth();
  const { chatId, setChatId } = useContext(ChatContext);

  const chats = data?.chats || [];

  return (
    <RootWrapper>
      <Typography
        sx={{
          mb: 1,
          mt: 2
        }}
        variant="h3"
      >
        {'Chats'}
      </Typography>

      {chats.map((chat) => {
        const { personId } = chat.persons.find(
          (person) => person.personId !== user?.id
        );

        const person = users.find(({ id }) => personId === id);

        return (
          <Box
            mt={2}
            key={chat.id}
            onClick={() => {
              setChatId(chat.id);
            }}
          >
            {
              <List disablePadding component="div">
                <ListItemWrapper selected={chat.id === chatId}>
                  <ListItemAvatar>
                    <Avatar src={person?.avatarPath} />
                  </ListItemAvatar>
                  <ListItemText
                    sx={{
                      mr: 1
                    }}
                    primaryTypographyProps={{
                      color: 'textPrimary',
                      variant: 'h5',
                      noWrap: true
                    }}
                    secondaryTypographyProps={{
                      color: 'textSecondary',
                      noWrap: true
                    }}
                    primary={person?.name + ' ' + person?.surname}
                    secondary={person?.position}
                  />
                </ListItemWrapper>
              </List>
            }
          </Box>
        );
      })}
    </RootWrapper>
  );
}

const RootWrapper = styled(Box)(
  ({ theme }) => `
        padding: ${theme.spacing(2.5)};
  `
);

const ListItemWrapper = styled(ListItemButton)(
  ({ theme }) => `
        &.MuiButtonBase-root {
            margin: ${theme.spacing(1)} 0;
        }
  `
);
