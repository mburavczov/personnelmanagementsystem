using System;

namespace Api.Entity
{
    public enum ProjectTaskStatus
    {
        New = 1,
        InProgress = 2,
        Wait = 3,
        Done = 4,
    }

    public static class ProjectTaskStatusExtension
    {
        public static string Name(this ProjectTaskStatus status)
        {
            return status switch
            {
                ProjectTaskStatus.New => "New",
                ProjectTaskStatus.InProgress => "InProgress",
                ProjectTaskStatus.Wait => "Wait",
                ProjectTaskStatus.Done => "Done",
                _ => throw new ArgumentOutOfRangeException(nameof(status), status, null)
            };
        }
    }
}