export interface LoginRequest {
  login: string;
  pass: string;
}

export interface LoginResponse {
  token: string;
}
