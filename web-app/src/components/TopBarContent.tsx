import { Box, Avatar, Typography, styled } from '@mui/material';
import { formatDistance, subMinutes } from 'date-fns';

const RootWrapper = styled(Box)(
  ({ theme }) => `
        @media (min-width: ${theme.breakpoints.values.md}px) {
          display: flex;
          align-items: center;
          justify-content: space-between;
      }
`
);

function TopBarContent() {
  return (
    <>
      <RootWrapper>
        <Box
          sx={{
            display: { sm: 'flex' }
          }}
          alignItems="center"
        >
          <Avatar
            variant="rounded"
            sx={{
              width: 50,
              height: 50
            }}
            alt="Zain Baptista"
            src="/static/images/avatars/1.jpg"
          />
          <Box
            sx={{
              pl: { sm: 1.5 },
              pt: { xs: 1.5, sm: 0 }
            }}
          >
            <Typography variant="h4" gutterBottom>
              Zain Baptista
            </Typography>
            <Typography variant="subtitle2">
              {formatDistance(subMinutes(new Date(), 8), new Date(), {
                addSuffix: true
              })}
            </Typography>
          </Box>
        </Box>
        <Box
          sx={{
            mt: { xs: 3, md: 0 }
          }}
        ></Box>
      </RootWrapper>
    </>
  );
}

export default TopBarContent;
