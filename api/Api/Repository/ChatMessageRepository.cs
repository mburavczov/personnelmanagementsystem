﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Api.Entity;

namespace Api.Repository
{
    public class ChatMessageRepository : BaseRepository<ChatMessage>
    {
        public ChatMessageRepository(MainContext context) : base(context)
        {
        }


        public List<ChatMessage> GetByChatId(int chatId)
        {
            return DbSet.Include(x => x.Sender)
                .Where(x => x.ChatId == chatId)
                .OrderByDescending(x => x.CreatedAt)
                .ToList();
        }
        
        public ChatMessage GetLastByChatId(int chatId)
        {
            return DbSet.Include(x => x.Sender)
                .Where(x => x.ChatId == chatId)
                .OrderByDescending(x => x.CreatedAt)
                .FirstOrDefault();
        }
    }
}