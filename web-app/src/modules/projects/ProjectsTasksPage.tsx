import { Footer, StandartPageHeader } from '@/components';
import { useAuth, useBooleanState, useRouterQueryId } from '@/hooks';
import { UserRoles } from '@/models/enums';
import { Button, Grid } from '@mui/material';
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import { AddTaskPopup } from '@/modules/projects/popups';
import { useProjectDetailQuery } from '@/api';
import { ProjectsBoard } from '@/modules/projects/task-detail/ProjectsBoard';

export function ProjectsTasksPage() {
  const { user } = useAuth();
  const projectId = useRouterQueryId();
  const { data: project } = useProjectDetailQuery({ projectId });
  const [addTaskOpen, toggleAddTaskOpen] = useBooleanState();

  const isActionAddTaskToProject =
    user?.role === UserRoles.ADMIN || user?.role === UserRoles.MANAGER;

  if (!project) return null;
  return (
    <>
      <StandartPageHeader
        title={`Задачи проекта "${project.name}"`}
        caption={`Это доска задач`}
        action={
          isActionAddTaskToProject && (
            <Button
              sx={{
                m: 1
              }}
              onClick={() => toggleAddTaskOpen()}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Добавить задачу
            </Button>
          )
        }
      />

      <Grid
        sx={{ px: 4 }}
        container
        direction="row"
        justifyContent="center"
        alignItems="stretch"
        spacing={3}
      >
        <Grid item xs={12}>
          <ProjectsBoard />
        </Grid>
      </Grid>

      <Footer />

      {isActionAddTaskToProject && (
        <AddTaskPopup
          projectId={projectId}
          open={addTaskOpen}
          toggleOpen={toggleAddTaskOpen}
        />
      )}
    </>
  );
}
