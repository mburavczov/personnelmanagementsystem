import { DialogPopup } from '@/components';
import {
  Autocomplete,
  Button,
  CircularProgress,
  TextField,
  Typography
} from '@mui/material';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { usePersonsQuery, useUpdateTaskMutation } from '@/api';
import { TaskStatuses, UserRoles } from '@/models/enums';
import { useQueryClient } from 'react-query';
import { Task } from '@/models';
import { taskStatusesMapping } from '@/utils';

interface UpdateTaskPopupProps {
  open: boolean;
  toggleOpen: () => void;
  projectId: number;
  task: Task;
}

export function UpdateTaskPopup({
  open,
  toggleOpen,
  task,
  projectId
}: UpdateTaskPopupProps) {
  const queryClient = useQueryClient();
  const { data: users = [] } = usePersonsQuery({
    role: UserRoles.EMPLOYEE
  });
  const updateTask = useUpdateTaskMutation({
    onSuccess: () => {
      toggleOpen();
      queryClient.invalidateQueries(['project', projectId]);
    }
  });
  const formik = useFormik({
    initialValues: {
      id: task.id,
      name: task.name,
      description: task.description,
      assignedId: task.assigned.id,
      status: task.status
    },
    validationSchema: Yup.object({
      id: Yup.number().required('Выберите проект.'),
      name: Yup.string().required('Введите название задачи.'),
      description: Yup.string().required('Введите описание задачи.'),
      assignedId: Yup.number().required('Выберите ответственного.'),
      status: Yup.string().required('Выберите статус.')
    }),
    onSubmit: (values): void => {
      updateTask(values);
    }
  });

  return (
    <>
      <DialogPopup
        header={
          <Typography variant="h4" gutterBottom>
            Добавление задачи в проект
          </Typography>
        }
        actions={
          <>
            <Button color="secondary" onClick={toggleOpen}>
              Отмена
            </Button>
            <Button
              onClick={() => formik.handleSubmit()}
              type="submit"
              startIcon={
                formik.isSubmitting ? <CircularProgress size="1rem" /> : null
              }
              disabled={formik.isSubmitting}
              variant="contained"
            >
              Изменить
            </Button>
          </>
        }
        open={open}
        onClose={toggleOpen}
      >
        <TextField
          error={Boolean(formik.touched.name && formik.errors.name)}
          fullWidth
          margin="normal"
          helperText={formik.touched.name && formik.errors.name}
          label={'Название задачи'}
          name="name"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.name}
          variant="outlined"
        />
        <TextField
          error={Boolean(
            formik.touched.description && formik.errors.description
          )}
          fullWidth
          margin="normal"
          helperText={formik.touched.description && formik.errors.description}
          label={'Описание задачи'}
          name="description"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.description}
          variant="outlined"
          multiline
        />
        <Autocomplete
          options={Object.values(TaskStatuses).map((status) => ({
            label: taskStatusesMapping[status].label,
            value: status
          }))}
          onChange={(_, selectedOption) => {
            formik.setFieldValue('status', selectedOption?.value as number);
          }}
          defaultValue={{
            label: taskStatusesMapping[formik.values.status].label,
            value: formik.values.status
          }}
          renderInput={(params) => (
            <TextField
              fullWidth
              margin="normal"
              {...params}
              label={'Статус задачи'}
              error={Boolean(formik.touched.status && formik.errors.status)}
              helperText={formik.touched.status && formik.errors.status}
            />
          )}
        />
        <Autocomplete
          options={users.map((user) => ({
            label: `${user.name} ${user.surname}`,
            value: user.id
          }))}
          onChange={(_, selectedOption) => {
            formik.setFieldValue('assignedId', selectedOption?.value as number);
          }}
          defaultValue={{
            label: `${task.assigned.name} ${task.assigned.surname}`,
            value: formik.values.assignedId
          }}
          renderInput={(params) => (
            <TextField
              fullWidth
              margin="normal"
              {...params}
              label={'Ответственный'}
              error={Boolean(
                formik.touched.assignedId && formik.errors.assignedId
              )}
              helperText={formik.touched.assignedId && formik.errors.assignedId}
            />
          )}
        />
      </DialogPopup>
    </>
  );
}
