import { useQueryClient } from 'react-query';
import { useAddTimelogMutation } from '@/api';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { DialogPopup } from '@/components';
import {
  Button,
  CircularProgress,
  Stack,
  TextField,
  Typography
} from '@mui/material';

interface AddTimelogPopupProps {
  open: boolean;
  toggleOpen: () => void;
  taskId: number;
  personId: number;
}

export function AddTimelogPopup({
  open,
  toggleOpen,
  taskId,
  personId
}: AddTimelogPopupProps) {
  const queryClient = useQueryClient();
  const addTimelog = useAddTimelogMutation({
    onSuccess: () => {
      toggleOpen();
      queryClient.invalidateQueries(['task', taskId]);
    }
  });
  const formik = useFormik({
    initialValues: {
      taskId,
      personId,
      minutes: 0,
      hours: 0,
      comment: ''
    },
    validationSchema: Yup.object({
      minutes: Yup.number().required('Выберите кол-во минут.'),
      hours: Yup.number().required('Введите кол-во часов.'),
      comment: Yup.string().required('Опишите, что Вы делали.')
    }),
    onSubmit: ({ taskId, personId, minutes, hours, comment }): void => {
      addTimelog({ taskId, personId, minutes: hours * 60 + minutes, comment });
    }
  });

  return (
    <>
      <DialogPopup
        header={
          <Typography variant="h4" gutterBottom>
            Добавление отчета в задачу
          </Typography>
        }
        actions={
          <>
            <Button color="secondary" onClick={toggleOpen}>
              Отмена
            </Button>
            <Button
              onClick={() => formik.handleSubmit()}
              type="submit"
              startIcon={
                formik.isSubmitting ? <CircularProgress size="1rem" /> : null
              }
              disabled={formik.isSubmitting}
              variant="contained"
            >
              Добавить
            </Button>
          </>
        }
        open={open}
        onClose={toggleOpen}
      >
        <Stack direction="row" spacing={1} alignItems="center">
          <TextField
            error={Boolean(formik.touched.hours && formik.errors.hours)}
            fullWidth
            helperText={formik.touched.hours && formik.errors.hours}
            label={'Часы'}
            name="hours"
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            value={formik.values.hours}
            variant="outlined"
            type="number"
          />
          <TextField
            error={Boolean(formik.touched.minutes && formik.errors.minutes)}
            fullWidth
            helperText={formik.touched.minutes && formik.errors.minutes}
            label={'Минуты'}
            name="minutes"
            onBlur={formik.handleBlur}
            onChange={formik.handleChange}
            value={formik.values.minutes}
            variant="outlined"
            type="number"
          />
        </Stack>
        <TextField
          error={Boolean(formik.touched.comment && formik.errors.comment)}
          fullWidth
          margin="normal"
          helperText={formik.touched.comment && formik.errors.comment}
          label={'Комментарий'}
          name="comment"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.comment}
          variant="outlined"
          multiline
        />
      </DialogPopup>
    </>
  );
}
