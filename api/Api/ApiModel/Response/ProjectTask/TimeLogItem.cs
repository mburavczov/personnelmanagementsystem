﻿using System;
using Api.ApiModel.Response.Profile;

namespace Api.ApiModel.Response.ProjectTask
{
    public class TimeLogItem
    {
        public int Id { get; set; }
        public int Minutes { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedAt { get; set; }
        public PersonItem Person { get; set; }
    }
}