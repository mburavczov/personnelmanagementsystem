﻿using System.Collections.Generic;
using System.Linq;
using Api.ApiModel.Request.Project;
using Api.ApiModel.Response.Project;
using Api.Entity;
using Api.Helper;
using Api.Repository;
using Api.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// Methods for manage projects
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectController : Controller
    {
        private readonly ProjectRepository _projectRepository;
        private readonly PersonRepository _personRepository;

        public ProjectController(ProjectRepository projectRepository, PersonRepository personRepository)
        {
            _projectRepository = projectRepository;
            _personRepository = personRepository;
        }

        /// <summary>
        /// Create new project
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="400">Empty project name</response>
        [Authorize(Roles = "admin,manager")]
        [HttpPost("Create")]
        public IActionResult Create([FromBody] CreateProjectRequest data)
        {
            if (string.IsNullOrEmpty(data.Name))
            {
                return BadRequest();
            }

            var manager = _personRepository.GetById(data.ManagerId);
            if (manager == null)
            {
                return NotFound("manager not found");
            }

            var customer = _personRepository.GetById(data.CustomerId);
            if (customer == null)
            {
                return NotFound("customer not found");
            }

            var project = _projectRepository.Add(new Project
            {
                Name = data.Name,
                Description = data.Description,
                ManagerId = data.ManagerId,
                CustomerId = data.CustomerId,
            });
            project.Persons.Add(manager);
            project.Persons.Add(customer);
            _projectRepository.Update(project);

            return Ok();
        }

        /// <summary>
        /// Get project by id
        /// </summary>
        /// <param name="projectId"></param>
        [Authorize]
        [HttpGet("Get")]
        [ProducesResponseType(typeof(ProjectItem), 200)]
        public IActionResult Get(int projectId)
        {
            var project = _projectRepository.GetById(projectId);
            if (project == null)
            {
                return NotFound();
            }

            return Ok(MapProject(project));
        }

        /// <summary>
        /// Add new task to project
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="404">Project not found</response>
        [Authorize(Roles = "admin,manager")]
        [HttpPost("AddTask")]
        public IActionResult AddTask(AddTaskRequest data)
        {
            var project = _projectRepository.GetById(data.ProjectId);
            if (project == null)
            {
                return NotFound();
            }
            
            project.ProjectTasks.Add(new ProjectTask
            {
                Name = data.TaskName,
                Description = data.TaskDescription,
                AssignedId = data.AssignedId,
                Status = ProjectTaskStatus.New,
            });
            
            _projectRepository.Update(project);

            return Ok();
        }

        /// <summary>
        /// Add employee to project
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="404">Project person not found</response>
        [Authorize(Roles = "admin,manager")]
        [HttpPost("AddMember")]
        public IActionResult AddMember(AddMemberRequest data)
        {
            var person = _personRepository.GetById(data.PersonId);
            if (person == null)
            {
                return NotFound();
            }

            var project = _projectRepository.GetById(data.ProjectId);
            if (project == null)
            {
                return NotFound();
            }
            
            project.Persons.Add(person);
            
            _projectRepository.Update(project);

            return Ok();
        }
        
        /// <summary>
        /// Update project
        /// </summary>
        /// <response code="200">Ok</response>
        /// <response code="404">Project not found</response>
        [Authorize(Roles = "admin")]
        [HttpPost("Update")]
        public IActionResult Update(UpdateProjectRequest data)
        {
            var project = _projectRepository.GetById(data.ProjectId);
            if (project == null)
            {
                return NotFound();
            }

            project.Name = data.Name;
            project.Description = data.Description;
            project.ManagerId = data.ManagerId;
            project.CustomerId = data.CustomerId;
            
            _projectRepository.Update(project);

            return Ok();
        }

        /// <summary>
        /// Get projects by person id
        /// </summary>
        /// <param name="personId">If not specified, the current person is used</param>
        /// <response code="200">Ok</response>
        /// <response code="404">Project person not found</response>
        [Authorize(Roles = "admin,manager,employee")]
        [HttpGet("GetPersonProjects")]
        [ProducesResponseType(typeof(GetPersonProjectsResponse), 200)]
        public IActionResult GetPersonProjects(int? personId)
        {
            if (!personId.HasValue)
            {
                personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            }

            var person = _personRepository.GetById(personId.Value);
            if (person == null)
            {
                return NotFound();
            }

            var projects = User?.IsInRole(PersonRole.Admin.Name()) == true 
                ? _projectRepository.GetAll() 
                : _projectRepository.GetByPersonId(personId.Value);

            return Ok(new GetPersonProjectsResponse { Projects = projects.Select(MapProject).ToList() });
        }

        /// <summary>
        /// Get projects statistic
        /// </summary>
        /// <response code="200">Ok</response>
        [Authorize(Roles = "admin")]
        [HttpGet("GetStat")]
        [ProducesResponseType(typeof(ProjectStatResponse), 200)]
        public IActionResult GetStat()
        {
            var projects = _projectRepository.GetAll();

            var items = projects.Select(x => new ProjectStatItem
            {
                ProjectName = x.Name,
                TotalTasks = x.ProjectTasks.Count,
                NewTasks = x.ProjectTasks.Count(t => t.Status == ProjectTaskStatus.New),
                InprogressTasks = x.ProjectTasks.Count(t => t.Status == ProjectTaskStatus.InProgress),
                WaitTasks = x.ProjectTasks.Count(t => t.Status == ProjectTaskStatus.Wait),
                DoneTasks = x.ProjectTasks.Count(t => t.Status == ProjectTaskStatus.Done),
                SpentTime = x.ProjectTasks.Select(t => t.TimeLogs.Sum(l => l.Minutes)).Sum()
            }).ToList();

            return Ok(new ProjectStatResponse { Items = items });
        }

        /// <summary>
        /// Get person statistic
        /// </summary>
        /// <response code="200">Ok</response>
        [Authorize(Roles = "admin")]
        [HttpGet("GetStatByPerson")]
        [ProducesResponseType(typeof(GetStatByPersonResponse), 200)]
        public IActionResult GetStatByPerson()
        {
            var persons = _personRepository.GetAllForStat();

            var items = persons.Select(x => new GetStatByPersonItem
            {
                PersonName = x.Name + " " + x.Surname,
                Items = x.ProjectTasks.GroupBy(t => t.ProjectId).Select(p => new ProjectStatItem
                {
                    ProjectName = p.First().Name,
                    TotalTasks = p.Count(),
                    NewTasks = p.Count(t => t.Status == ProjectTaskStatus.New),
                    InprogressTasks = p.Count(t => t.Status == ProjectTaskStatus.InProgress),
                    WaitTasks = p.Count(t => t.Status == ProjectTaskStatus.Wait),
                    DoneTasks = p.Count(t => t.Status == ProjectTaskStatus.Done),
                    SpentTime = p.Select(t => t.TimeLogs.Sum(l => l.Minutes)).Sum()
                }).ToList()
            }).ToList();

            return Ok(new GetStatByPersonResponse { Items = items });
        }

        private ProjectItem MapProject(Project project)
        {
            return new ProjectItem
            {
                Id = project.Id,
                Name = project.Name,
                Description = project.Description,
                Manager = PersonHelper.MapPerson(project.Manager),
                Customer = PersonHelper.MapPerson(project.Customer),
                Persons = project.Persons.Select(PersonHelper.MapPerson)?.ToList(),
                Tasks = project.ProjectTasks.Select(TaskHelper.MapProjectTask).ToList()
            };
        }
    }
}