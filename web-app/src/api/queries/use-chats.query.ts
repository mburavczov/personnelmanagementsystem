import { useQuery } from 'react-query';
import { client } from '@/api';

export type ChatsResponse = {
  chats: {
    id: number;
    name: string;
    lastMsg: string;
    date: Date;
    preview: string;
    persons: {
      personId: number;
      name: string;
    }[];
  }[];
};

export function useChats() {
  return useQuery<ChatsResponse>(
    ['chats'],
    async () => {
      try {
        const dto = await client.get('Chat/Chats');
        return dto.data;
      } catch {
        console.log('fatal error');
      }
    },
    {}
  );
}
