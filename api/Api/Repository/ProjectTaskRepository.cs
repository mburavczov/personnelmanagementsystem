﻿using System.Collections.Generic;
using System.Linq;
using Api.Entity;
using Microsoft.EntityFrameworkCore;

namespace Api.Repository
{
    public class ProjectTaskRepository : BaseRepository<ProjectTask>
    {
        public ProjectTaskRepository(MainContext context) : base(context)
        {
        }

        public ProjectTask GetById(int id)
        {
            return DbSet
                .Include(x => x.Assigned)
                .Include(x => x.TimeLogs).ThenInclude(x => x.Person)
                .FirstOrDefault(x => x.Id == id);
        }

        public List<ProjectTask> GetByPersonId(int personId)
        {
            return DbSet
                .Include(x => x.Assigned)
                .Where(x => x.AssignedId == personId)
                .ToList();
        }
    }
}