import { Box, Container, Grid, Typography, styled } from '@mui/material';
import { Logo } from '@/components';

export function Hero() {
  return (
    <Container maxWidth="lg">
      <Grid
        spacing={{ xs: 6, md: 10 }}
        justifyContent="center"
        alignItems="center"
        container
      >
        <Grid item md={6} pr={{ xs: 0, md: 3 }}>
          <Typography
            sx={{
              fontSize: 40,
              mb: 2
            }}
            variant="h1"
          >
            Проект команды "Феникс"
          </Typography>
          <Typography
            sx={{
              fontSize: 17,
              lineHeight: 1.5,
              pb: 4
            }}
            variant="h4"
            color="text.secondary"
            fontWeight="normal"
          >
            #TulaHack2022 - это 40 часов упорной работы над IT-проектом на
            заданную работодателями тему, начиная с формирования команды и
            генерации идеи, и заканчивая презентацией прототипа (MVP) продукта
            перед экспертами.
          </Typography>
          <ListItemWrapper sx={{ mt: 5, mb: 2 }}>
            <Brand>
              <Logo />
            </Brand>
            <Typography variant="h6">
              <b>Кейс</b>
              <Typography component="span" variant="subtitle2">
                {' '}
                - Информационная система "Личный кабинет сотрудника".
              </Typography>
            </Typography>
          </ListItemWrapper>
          <ListItemWrapper sx={{ mt: 5, mb: 2 }}>
            <Brand>
              <Logo />
            </Brand>
            <Typography variant="h6">
              <b>Стэк технологий</b>
              <Typography component="span" variant="subtitle2">
                {' '}
                - C#, ASP.NET Core 5; TypeScript, React, NextJS, MaterialUI;
                Docker, Gitlab CI/CD.
              </Typography>
            </Typography>
          </ListItemWrapper>
          <ListItemWrapper sx={{ mt: 5, mb: 2 }}>
            <Brand>
              <Logo />
            </Brand>
            <Typography variant="h6">
              <b>Состав команды</b>
              <Typography component="span" variant="subtitle2">
                {' '}
                - Михаил Буравцов, Дмитрий Рассохин, Дмитрий Петровичев.
              </Typography>
            </Typography>
          </ListItemWrapper>
        </Grid>
        <Grid item md={6}>
          <ImgWrapper>
            <img alt="TulaHack" src="/static/images/tulahack.jpg" />
          </ImgWrapper>
        </Grid>
      </Grid>
    </Container>
  );
}

const ImgWrapper = styled(Box)(
  ({ theme }) => `
    position: relative;
    z-index: 5;
    width: 100%;
    overflow: hidden;
    border-radius: ${theme.general.borderRadiusLg};
    box-shadow: 0 0rem 14rem 0 rgb(255 255 255 / 20%), 0 0.8rem 2.3rem rgb(111 130 156 / 3%), 0 0.2rem 0.7rem rgb(17 29 57 / 15%);

    img {
      display: block;
      width: 100%;
    }
  `
);

const ListItemWrapper = styled(Box)`
  display: flex;
  align-items: center;
`;

const Brand = styled(Box)(
  ({ theme }) => `
    width: ${theme.spacing(8)};
    height: ${theme.spacing(8)};
    border-radius: ${theme.general.borderRadius};
    background-color: ${theme.colors.info.lighter};
    flex-shrink: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: ${theme.spacing(2)};

    img {
      width: 60%;
      height: 60%;
      display: block;
    }
`
);
