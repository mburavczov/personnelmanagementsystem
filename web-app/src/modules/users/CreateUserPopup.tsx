import { useCreateUserMutation } from '@/api';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { DialogPopup } from '@/components';
import {
  Autocomplete,
  Button,
  CircularProgress,
  Divider,
  TextField,
  Typography
} from '@mui/material';
import { UserRoles } from '@/models/enums';
import { userRoleMapping } from '@/utils';
import { useQueryClient } from 'react-query';

interface CreateUserPopupProps {
  open: boolean;
  toggleOpen: () => void;
}

export function CreateUserPopup({ open, toggleOpen }: CreateUserPopupProps) {
  const queryClient = useQueryClient();
  const createUser = useCreateUserMutation({
    onSuccess: () => {
      toggleOpen();
      queryClient.invalidateQueries('persons');
    }
  });
  const formik = useFormik({
    initialValues: {
      login: '',
      pass: '',
      name: '',
      role: '' as UserRoles,
      surname: '',
      phone: '',
      about: '',
      position: ''
    },
    validationSchema: Yup.object({
      login: Yup.string()
        .email('Введен некорректный Email.')
        .max(255)
        .required('Не введен Email.'),
      pass: Yup.string().max(255).required('Не введен пароль.'),
      name: Yup.string().required('Не введено имя.'),
      surname: Yup.string().required('Не введена фамилия.'),
      position: Yup.string().required('Не введена должность.'),
      role: Yup.string().required('Не выбрана роль.')
    }),
    onSubmit: async (values): Promise<void> => {
      await createUser(values);
    }
  });

  return (
    <>
      <DialogPopup
        header={
          <Typography variant="h4" gutterBottom>
            Создание нового пользователя
          </Typography>
        }
        actions={
          <>
            <Button color="secondary" onClick={toggleOpen}>
              Отмена
            </Button>
            <Button
              onClick={() => formik.handleSubmit()}
              type="submit"
              startIcon={
                formik.isSubmitting ? <CircularProgress size="1rem" /> : null
              }
              disabled={formik.isSubmitting}
              variant="contained"
            >
              Создать
            </Button>
          </>
        }
        open={open}
        onClose={toggleOpen}
      >
        <TextField
          error={Boolean(formik.touched.login && formik.errors.login)}
          fullWidth
          margin="normal"
          helperText={formik.touched.login && formik.errors.login}
          label={'Email'}
          name="login"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.login}
          variant="outlined"
        />
        <TextField
          error={Boolean(formik.touched.pass && formik.errors.pass)}
          fullWidth
          margin="normal"
          helperText={formik.touched.pass && formik.errors.pass}
          label={'Пароль'}
          name="pass"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.pass}
          variant="outlined"
        />
        <Divider />
        <TextField
          error={Boolean(formik.touched.name && formik.errors.name)}
          fullWidth
          margin="normal"
          helperText={formik.touched.name && formik.errors.name}
          label={'Имя'}
          name="name"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.name}
          variant="outlined"
        />
        <TextField
          error={Boolean(formik.touched.surname && formik.errors.surname)}
          fullWidth
          margin="normal"
          helperText={formik.touched.surname && formik.errors.surname}
          label={'Фамилию'}
          name="surname"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.surname}
          variant="outlined"
        />
        <Autocomplete
          options={Object.values(UserRoles).map((role) => ({
            label: userRoleMapping[role].label,
            value: role
          }))}
          onChange={(_, selectedOption) => {
            formik.setFieldValue('role', selectedOption?.value as number);
          }}
          renderInput={(params) => (
            <TextField
              fullWidth
              margin="normal"
              {...params}
              label={'Роль сотрудника'}
              error={Boolean(formik.touched.role && formik.errors.role)}
              helperText={formik.touched.role && formik.errors.role}
            />
          )}
        />
        <TextField
          error={Boolean(formik.touched.position && formik.errors.position)}
          fullWidth
          margin="normal"
          helperText={formik.touched.position && formik.errors.position}
          label={'Должность'}
          name="position"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.position}
          variant="outlined"
        />
        <TextField
          error={Boolean(formik.touched.phone && formik.errors.phone)}
          fullWidth
          margin="normal"
          helperText={formik.touched.phone && formik.errors.phone}
          label={'Номер телефона'}
          name="phone"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.phone}
          variant="outlined"
        />
        <TextField
          error={Boolean(formik.touched.about && formik.errors.about)}
          fullWidth
          margin="normal"
          helperText={formik.touched.about && formik.errors.about}
          label={'О себе'}
          name="about"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.about}
          variant="outlined"
          multiline
        />
      </DialogPopup>
    </>
  );
}
