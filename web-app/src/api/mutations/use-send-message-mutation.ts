import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

export interface SendMessageRequest {
  chatId: number;
  text: string;
}

export function useSendMessageMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, SendMessageRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, SendMessageRequest>(
    (data) => client.post('Chat/SendMessage', data),
    options
  );

  return mutation.mutateAsync;
}
