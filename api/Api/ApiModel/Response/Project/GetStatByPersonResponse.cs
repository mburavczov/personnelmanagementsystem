﻿using System.Collections.Generic;

namespace Api.ApiModel.Response.Project
{
    public class GetStatByPersonResponse
    {
        public List<GetStatByPersonItem> Items { get; set; }
    }

    public class GetStatByPersonItem
    {
        public string PersonName { get; set; }
        public List<ProjectStatItem> Items { get; set; }
    }
}