import Head from 'next/head';
import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';
import { TaskDetailPage } from '@/modules/projects';

function ProjectTask() {
  return (
    <>
      <Head>
        <title>Задача</title>
      </Head>
      <TaskDetailPage />
    </>
  );
}

ProjectTask.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default ProjectTask;
