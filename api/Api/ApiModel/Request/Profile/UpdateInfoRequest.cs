﻿namespace Api.ApiModel.Request.Profile
{
    public class UpdateInfoRequest
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string Position { get; set; }
        public string About { get; set; }
        public string AvatarPath { get; set; }
    }
}