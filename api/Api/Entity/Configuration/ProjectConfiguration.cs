﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Api.Entity.Configuration
{
    public class ProjectConfiguration : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.HasKey(x => x.Id);
            
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.Description).IsRequired();
            builder.Property(x => x.ManagerId).IsRequired();
            builder.Property(x => x.CustomerId).IsRequired();
            builder.Ignore(x => x.Customer);
            builder.Ignore(x => x.Manager);

            builder.HasMany(x => x.Persons)
                .WithMany(x => x.Projects)
                .UsingEntity(x => x.ToTable("PersonProject"));
        }
    }
}