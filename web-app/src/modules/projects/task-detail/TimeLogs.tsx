import { useTaskDetailQuery } from '@/api';
import { useAuth, useBooleanState, useRouterQueryId } from '@/hooks';
import {
  Typography,
  Card,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  CardHeader,
  Button
} from '@mui/material';
import { formatDate, getHoursFromMinute } from '@/utils';
import { UserRoles } from '@/models/enums';
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import React from 'react';
import { AddTimelogPopup } from '@/modules/projects/popups/AddTimelogPopup';

export function TimeLogs() {
  const taskId = useRouterQueryId('taskId');
  const { data: task } = useTaskDetailQuery({ taskId });
  const [addTimelogOpen, toggleAddTimelogOpen] = useBooleanState();
  const { user } = useAuth();
  const isAddTimelog = user.role !== UserRoles.CUSTOMER;

  if (!user || !task) return null;

  return (
    <Card>
      <CardHeader
        title="Отчеты по задаче"
        action={
          isAddTimelog && (
            <Button
              sx={{
                m: 1
              }}
              size="small"
              onClick={() => toggleAddTimelogOpen()}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Добавить отчет
            </Button>
          )
        }
      />
      {task.timeLogs.length === 0 ? (
        <>
          <Typography
            sx={{
              py: 10
            }}
            variant="h3"
            fontWeight="normal"
            color="text.secondary"
            align="center"
          >
            Не найдено ни одного отчета
          </Typography>
        </>
      ) : (
        <>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Пользователь</TableCell>
                  <TableCell>Время</TableCell>
                  <TableCell>Дата</TableCell>
                  <TableCell>Комментарий</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {task.timeLogs.map((timeLog) => {
                  const { minutes, hours } = getHoursFromMinute(
                    timeLog.minutes
                  );

                  return (
                    <TableRow hover key={timeLog.id}>
                      <TableCell>
                        <Typography variant="h5">
                          {timeLog.person.name} {timeLog.person.surname}
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography>
                          {hours}:{minutes}
                        </Typography>
                      </TableCell>
                      <TableCell>
                        <Typography>{formatDate(timeLog.createdAt)}</Typography>
                      </TableCell>
                      <TableCell>
                        <Typography>{timeLog.comment}</Typography>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      )}

      {isAddTimelog && (
        <AddTimelogPopup
          open={addTimelogOpen}
          toggleOpen={toggleAddTimelogOpen}
          taskId={task.id}
          personId={user.id}
        />
      )}
    </Card>
  );
}
