import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

export interface AddTaskToProjectRequest {
  projectId: number;
  taskName: string;
  taskDescription: string;
  assignedId: number;
}

export function useAddTaskToProjectMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, AddTaskToProjectRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, AddTaskToProjectRequest>(
    (data) => client.post('Project/AddTask', data),
    options
  );

  return mutation.mutateAsync;
}
