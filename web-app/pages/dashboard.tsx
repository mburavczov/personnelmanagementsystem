import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';
import Head from 'next/head';
import { DashboardPage } from '@/modules/dashboard';

function Dashboard() {
  return (
    <>
      <Head>
        <title>TulaHack 2022</title>
      </Head>
      <DashboardPage />
    </>
  );
}

Dashboard.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default Dashboard;
