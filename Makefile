db-up:
	cd infrastructure && docker-compose up

db-down:
	cd infrastructure && docker-compose down

api-build:
	docker build -f api/Dockerfile -t api .

api-start:
	docker run -p 8080:80 --add-host=host.docker.internal:host-gateway --env-file api/.env.dev -it api

web-dev:
	cd web-app && npm run dev

web-i:
	cd web-app && npm install

