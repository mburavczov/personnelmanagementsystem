import { useQuery } from 'react-query';
import { client } from '@/api';
import { Task } from '@/models';

export interface GetTaskDetailRequest {
  taskId: number;
}

export type GetTaskDetailResponse = Task;

export function useTaskDetailQuery(params: GetTaskDetailRequest) {
  return useQuery<GetTaskDetailResponse>(
    ['task', ...Object.values(params)],
    async () => {
      try {
        const dto = await client.get('ProjectTask/GetTask', {
          params
        });
        return dto.data;
      } catch {
        console.log('fatal error');
      }
    },
    {}
  );
}
