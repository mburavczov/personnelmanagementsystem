process.env.NEXT_PUBLIC_API_BASE_URL = process.env.API_BASE_URL;

module.exports = {
  typescript: {
    ignoreBuildErrors: true
  }
};
