﻿using Api.Entity;

namespace Api.ApiModel.Request.ProjectTask
{
    public class UpdateRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? AssignedId { get; set; }
        public ProjectTaskStatus Status { get; set; }
    }
}