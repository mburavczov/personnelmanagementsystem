import type { ReactNode } from 'react';
import HomeIcon from '@mui/icons-material/Home';
import PersonIcon from '@mui/icons-material/Person';
import AssignmentIcon from '@mui/icons-material/Assignment';
import InfoIcon from '@mui/icons-material/Info';
import DashboardIcon from '@mui/icons-material/Dashboard';
import PeopleIcon from '@mui/icons-material/People';
import ChatIcon from '@mui/icons-material/Chat';

export interface MenuItem {
  link: string;
  icon: ReactNode;
  name: string;
}

export interface MenuItems {
  items: MenuItem[];
  heading: string;
}

export const menuItems: MenuItems[] = [
  {
    heading: 'Основные',
    items: [
      {
        name: 'Главная',
        link: '/dashboard',
        icon: HomeIcon
      },
      {
        name: 'Мои проекты',
        link: '/projects',
        icon: DashboardIcon
      },
      {
        name: 'Мои задачи',
        link: '/tasks',
        icon: AssignmentIcon
      },
      {
        name: 'Пользователи',
        link: '/users',
        icon: PeopleIcon
      }
    ]
  },
  {
    heading: 'Дополнительные',
    items: [
      {
        name: 'Профиль',
        link: '/profile',
        icon: PersonIcon
      },
      {
        name: 'Чаты',
        link: '/chat',
        icon: ChatIcon
      },
      {
        name: 'О проекте',
        link: '/',
        icon: InfoIcon
      }
    ]
  }
];
