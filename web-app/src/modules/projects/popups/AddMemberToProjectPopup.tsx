import { DialogPopup } from '@/components';
import {
  Autocomplete,
  Button,
  CircularProgress,
  TextField,
  Typography
} from '@mui/material';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useAddMemberToProjectMutation, usePersonsQuery } from '@/api';
import { UserRoles } from '@/models/enums';
import { useQueryClient } from 'react-query';

interface AddMemberToProjectPopupProps {
  open: boolean;
  toggleOpen: () => void;
  projectId: number;
}

export function AddMemberToProjectPopup({
  open,
  toggleOpen,
  projectId
}: AddMemberToProjectPopupProps) {
  const queryClient = useQueryClient();
  const { data: users = [] } = usePersonsQuery({
    role: UserRoles.EMPLOYEE
  });

  const addMemberToProject = useAddMemberToProjectMutation({
    onSuccess: () => {
      toggleOpen();
      queryClient.invalidateQueries('project', projectId);
    }
  });
  const formik = useFormik({
    initialValues: {
      projectId,
      personId: ''
    },
    validationSchema: Yup.object({
      projectId: Yup.number().required('Выберите проект.'),
      personId: Yup.number().required('Выберите участника.')
    }),
    onSubmit: (values): void => {
      addMemberToProject(values);
    }
  });

  return (
    <>
      <DialogPopup
        header={
          <Typography variant="h4" gutterBottom>
            Добавление участника в проект
          </Typography>
        }
        actions={
          <>
            <Button color="secondary" onClick={toggleOpen}>
              Отмена
            </Button>
            <Button
              onClick={() => formik.handleSubmit()}
              type="submit"
              startIcon={
                formik.isSubmitting ? <CircularProgress size="1rem" /> : null
              }
              disabled={formik.isSubmitting}
              variant="contained"
            >
              Добавить
            </Button>
          </>
        }
        open={open}
        onClose={toggleOpen}
      >
        <Autocomplete
          options={users.map((user) => ({
            label: `${user.name} ${user.surname}`,
            value: user.id
          }))}
          onChange={(_, selectedOption) => {
            formik.setFieldValue('personId', selectedOption?.value as number);
          }}
          renderInput={(params) => (
            <TextField
              fullWidth
              margin="normal"
              {...params}
              label={'Сотрудник'}
              error={Boolean(formik.touched.personId && formik.errors.personId)}
              helperText={formik.touched.personId && formik.errors.personId}
            />
          )}
        />
      </DialogPopup>
    </>
  );
}
