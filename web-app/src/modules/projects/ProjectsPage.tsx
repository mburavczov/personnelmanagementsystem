import { Footer, Link, StandartPageHeader } from '@/components';
import {
  Box,
  Button,
  Card,
  CircularProgress,
  circularProgressClasses,
  Divider,
  Grid,
  styled,
  Typography,
  useTheme
} from '@mui/material';
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import { useAuth, useBooleanState } from '@/hooks';
import { CreateProjectPopup } from '@/modules/projects/popups';
import { TaskStatuses, UserRoles } from '@/models/enums';
import { useProjectsQuery } from '@/api';
import React from 'react';

export function ProjectsPage() {
  const theme = useTheme();
  const { data } = useProjectsQuery({});
  const projects = data?.projects || [];
  const [createProjectOpen, toggleCreateProjectOpen] = useBooleanState();
  const { user } = useAuth();
  const isActionCreateProject = user.role === UserRoles.ADMIN;

  return (
    <>
      <StandartPageHeader
        title="Проекты"
        caption="Здесь отображается список проектов"
        action={
          isActionCreateProject && (
            <Button
              sx={{
                m: 1
              }}
              onClick={() => toggleCreateProjectOpen()}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Создать проект
            </Button>
          )
        }
      />

      <Grid
        sx={{ px: 4 }}
        container
        direction="row"
        justifyContent="center"
        alignItems="stretch"
        spacing={3}
      >
        <Grid item xs={12}>
          <Grid container spacing={3}>
            {projects.map((project) => {
              const countTasks = project.tasks.length;
              const countDoneTasks = project.tasks.filter(
                ({ status }) => status === TaskStatuses.DONE
              ).length;

              return (
                <Grid item xs={12} sm={6} key={project.id}>
                  <CardWrapper>
                    <Box
                      sx={{
                        position: 'relative',
                        zIndex: '2'
                      }}
                    >
                      <Box
                        p={2}
                        display="flex"
                        alignItems="flex-start"
                        justifyContent="space-between"
                      >
                        <Box>
                          <Typography component="span" variant="h5">
                            {project.name}
                          </Typography>

                          <Typography
                            sx={{
                              pt: 0.3
                            }}
                            variant="subtitle2"
                          >
                            Заказчик: {project.customer.name}{' '}
                            {project.customer.surname}
                          </Typography>
                          <Typography
                            sx={{
                              pt: 0.3
                            }}
                            variant="subtitle2"
                          >
                            Менеджер: {project.manager.name}{' '}
                            {project.manager.surname}
                          </Typography>
                        </Box>
                        <Box display="inline-flex" position="relative">
                          <Box
                            sx={{
                              animationDuration: '550ms',
                              position: 'absolute',
                              left: 0,
                              top: 0,
                              bottom: 0,
                              right: 0,
                              display: 'flex',
                              alignItems: 'center',
                              justifyContent: 'center'
                            }}
                          >
                            <Typography
                              sx={{
                                color: `${theme.colors.success.main}`
                              }}
                              variant="h5"
                            >
                              {countDoneTasks}/{countTasks}
                            </Typography>
                          </Box>
                          <CircularProgress
                            variant="determinate"
                            sx={{
                              color: theme.colors.success.lighter
                            }}
                            size={70}
                            thickness={2}
                            value={100}
                          />
                          <CircularProgress
                            size={70}
                            sx={{
                              animationDuration: '550ms',
                              position: 'absolute',
                              left: 0,
                              color: theme.colors.success.main,
                              top: 0,
                              [`& .${circularProgressClasses.circle}`]: {
                                strokeLinecap: 'round'
                              }
                            }}
                            thickness={2}
                            variant="determinate"
                            value={
                              countTasks !== 0
                                ? (countDoneTasks * 100) / countTasks
                                : 0
                            }
                          />
                        </Box>
                      </Box>
                      <Divider />
                      <Box
                        pl={2}
                        py={1}
                        pr={1}
                        display="flex"
                        alignItems="center"
                        justifyContent="flex-end"
                      >
                        <Button
                          component={Link}
                          href={`/projects/${project.id}`}
                        >
                          В проект
                        </Button>
                      </Box>
                    </Box>
                  </CardWrapper>
                </Grid>
              );
            })}
          </Grid>
        </Grid>
      </Grid>

      <Footer />

      {isActionCreateProject && (
        <CreateProjectPopup
          open={createProjectOpen}
          toggleOpen={toggleCreateProjectOpen}
        />
      )}
    </>
  );
}

const CardWrapper = styled(Card)(
  ({ theme }) => `

  position: relative;
  overflow: visible;

  &::after {
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    border-radius: inherit;
    z-index: 1;
    transition: ${theme.transitions.create(['box-shadow'])};
  }
      
    &.Mui-selected::after {
      box-shadow: 0 0 0 3px ${theme.colors.primary.main};
    }
  `
);
