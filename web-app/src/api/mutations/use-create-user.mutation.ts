import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';
import { UserRoles } from '@/models/enums';

export interface CreateUserRequest {
  login: string;
  pass: string;
  name: string;
  role: UserRoles;
  surname: string;
  phone?: string;
  about?: string;
  position: string;
}

export function useCreateUserMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, CreateUserRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, CreateUserRequest>(
    (data) => client.post('Account/CreateAccount', data),
    options
  );

  return mutation.mutateAsync;
}
