import Head from 'next/head';
import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';
import { MyTasksPage } from '@/modules/projects';

function Tasks() {
  return (
    <>
      <Head>
        <title>Мои задачи</title>
      </Head>
      <MyTasksPage />
    </>
  );
}

Tasks.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default Tasks;
