﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.ApiModel.Request.Profile;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Api.ApiModel.Response.Profile;
using Api.Entity;
using Api.Helper;
using Api.Repository;
using Api.Services;
using Api.Settings;

namespace Api.Controllers
{
    /// <summary>
    /// Methods for manage profile
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class ProfileController : Controller
    {
        private readonly PersonRepository _personRepository;
        private readonly ImageService _imageService;

        public ProfileController(PersonRepository personRepository, ImageService imageService)
        {
            _personRepository = personRepository;
            _imageService = imageService;
        }


        /// <summary>
        /// Get profile info
        /// </summary>
        /// <param name="personId">If not specified, the current id is used</param>
        /// <response code="404">Person not found</response>
        [Authorize(Roles = "admin,manager,employee")]
        [HttpGet("Info")]
        [ProducesResponseType(typeof(PersonItem), 200)]
        public async Task<IActionResult> Info(int? personId)
        {
            if (!personId.HasValue)
            {
                personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            }

            var person = _personRepository.GetById(personId.Value);
            if (person == null)
            {
                return NotFound();
            }

            return Ok(PersonHelper.MapPerson(person));
        }
        
        /// <summary>
        /// Update profile info
        /// </summary>
        /// <response code="404">Person not found</response>
        /// <response code="403">Forbidden</response>
        [Authorize(Roles = "admin,manager,employee")]
        [HttpPost("UpdateInfo")]
        [ProducesResponseType(typeof(UpdateAvatarResponse), 200)]
        public async Task<IActionResult> UpdateInfo(UpdateInfoRequest data)
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            var person = _personRepository.GetById(personId);
            if (person == null)
            {
                return NotFound();
            }
            
            if (User?.IsInRole(PersonRole.Employee.Name()) == true && personId != data.PersonId)
            {
                return Forbid();
            }

            person.Name = data.Name;
            person.Surname = data.Surname;
            person.Phone = data.Phone;
            person.Position = data.Position;
            person.About = data.About;
            person.AvatarPath = data.AvatarPath?.Split(new []{'/'})?.Last(); //  hot fix ¯\_(ツ)_/¯
            
            _personRepository.Update(person);

            return Ok();
        }

        /// <summary>
        /// Update profile avatar
        /// </summary>
        /// <response code="404">Person not found</response>
        /// <response code="400">Zero or more than 1 files in request</response>
        [Authorize]
        [HttpPost("UpdateAvatar")]
        [ProducesResponseType(typeof(UpdateAvatarResponse), 200)]
        public async Task<IActionResult> UpdateAvatar()
        {
            var personId = int.Parse(User?.FindFirst(x => x.Type == Constants.ClaimTypeUserId)?.Value);
            var person = _personRepository.GetById(personId);
            if (person == null)
            {
                return NotFound();
            }

            if (Request.Form.Files.Count != 1)
            {
                return BadRequest();
            }

            string filePath;
            using (var fs = Request.Form.Files[0].OpenReadStream())
            {
                filePath = await _imageService.Upload(fs);
            }

            person.AvatarPath = filePath;
            _personRepository.Update(person);
            
            return Ok(new UpdateAvatarResponse
            {
                Link = filePath
            });
        }

        /// <summary>
        /// Get all persons
        /// </summary>
        [Authorize(Roles = "admin,manager,employee")]
        [HttpGet("Persons")]
        [ProducesResponseType(typeof(List<PersonItem>), 200)]
        public IActionResult Persons(PersonRole? role)
        {
            var persons = _personRepository.GetByRole(role);

            return Ok(persons.Select(PersonHelper.MapPerson).ToList());
        }
    }
}