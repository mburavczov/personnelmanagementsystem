﻿using System;
using System.Collections.Generic;

namespace Api.Entity
{
    public class Chat
    {
        public Chat()
        {
            Persons = new List<Person>();
            Messages = new List<ChatMessage>();
        }
        
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        
        public virtual ICollection<Person> Persons { get; set; }
        public virtual ICollection<ChatMessage> Messages { get; set; }
    }
}