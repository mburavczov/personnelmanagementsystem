import { UserRoles } from '@/models/enums';

export interface User {
  id: number;
  login: string;
  role: UserRoles;
  state: string;
  name: string;
  surname: string;
  phone: string;
  about: string;
  position: string;
  avatarPath: string;
}
