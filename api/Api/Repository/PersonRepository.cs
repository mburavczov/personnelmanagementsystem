﻿using System.Collections.Generic;
using System.Linq;
using Api.Entity;
using Microsoft.EntityFrameworkCore;

namespace Api.Repository
{
    public class PersonRepository : BaseRepository<Person>
    {
        public PersonRepository(MainContext context) : base(context)
        {
        }

        
        public Person Get(string login)
        {
            return DbSet.FirstOrDefault(x => x.Login == login);
        }
        
        public List<Person> GetAll()
        {
            return DbSet.ToList();
        }
        
        public List<Person> GetAllForStat()
        {
            return DbSet
                .Include(x => x.ProjectTasks).ThenInclude(x => x.Project)
                .Include(x => x.ProjectTasks).ThenInclude(x => x.TimeLogs)
                .ToList();
        }
        
        public List<Person> GetByRole(PersonRole? role)
        {
            return DbSet.Where(x => role == null || x.Role == role).ToList();
        }

        public Person GetById(int id)
        {
            return DbSet.FirstOrDefault(x => x.Id == id);
        }

        public List<Person> GetByIds(int[] ids)
        {
            return DbSet.Where(x => ids.Contains(x.Id)).ToList();
        }
    }
}