﻿using System;
using Api.ApiModel.Response.Profile;
using Api.Entity;

namespace Api.Helper
{
    public class PersonHelper
    {
        public static PersonRole ParseRole(string role)
        {
            switch (role)
            {
                case "admin":
                    return PersonRole.Admin;
                case "manager":
                    return PersonRole.Manager;
                case "employee":
                    return PersonRole.Employee;
                case "customer":
                    return PersonRole.Customer;
                default:
                    throw new ArgumentException("unknown role");
            }
        }

        public static PersonItem MapPerson(Person person)
        {
            if (person == null)
            {
                return null;
            }
            
            var info = new PersonItem
            {
                Id = person.Id,
                Login = person.Login,
                Role = person.Role.Name(),
                State = person.State.Name(),
                Name = person.Name,
                Surname = person.Surname,
                Phone = person.Phone,
                About = person.About,
                Position = person.Position,
                AvatarPath = string.IsNullOrEmpty(person.AvatarPath) 
                    ? string.Empty 
                    : $"https://5936355c-e01e-46cf-beb2-e1a983dbdb2b.selcdn.net/{person.AvatarPath}"
            };

            return info;
        }
    }
}