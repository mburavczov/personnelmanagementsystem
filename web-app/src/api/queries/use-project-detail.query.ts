import { useQuery } from 'react-query';
import { client } from '@/api';
import { Project } from '@/models';

export interface GetProjectDetailRequest {
  projectId: number;
}

export type GetProjectDetailResponse = Project;

export function useProjectDetailQuery(params: GetProjectDetailRequest) {
  return useQuery<GetProjectDetailResponse>(
    ['project', ...Object.values(params)],
    async () => {
      try {
        const dto = await client.get('Project/Get', {
          params
        });
        return dto.data;
      } catch {
        console.log('fatal error');
      }
    },
    {}
  );
}
