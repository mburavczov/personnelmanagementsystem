export enum TaskStatuses {
  NEW = 'New',
  IN_PROGRESS = 'InProgress',
  WAIT = 'Wait',
  DONE = 'Done'
}
