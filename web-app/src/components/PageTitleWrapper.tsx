import { ReactNode } from 'react';
import { Box } from '@mui/material';

interface PageTitleWrapperProps {
  children: ReactNode;
}

export function PageTitleWrapper({ children }: PageTitleWrapperProps) {
  return (
    <>
      <Box p={4} className="MuiPageTitle-wrapper">
        {children}
      </Box>
    </>
  );
}
