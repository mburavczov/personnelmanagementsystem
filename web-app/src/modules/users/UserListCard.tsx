import { User } from '@/models';
import {
  Avatar,
  Box,
  Button,
  Card,
  Divider,
  Grid,
  styled,
  Typography
} from '@mui/material';
import { userRoleMapping } from '@/utils';
import { useAuth } from '@/hooks';
import { useRouter } from 'next/router';
import { useQueryClient } from 'react-query';
import { useCreateChatMutation } from '@/api';
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import React from 'react';

interface UserListCardProps {
  userCard: User;
}

export function UserListCard({ userCard }: UserListCardProps) {
  const { user } = useAuth();
  const router = useRouter();
  const queryClient = useQueryClient();
  const createChat = useCreateChatMutation({
    onSuccess: () => {
      queryClient.invalidateQueries('chats');
      router.push('chat');
    }
  });

  return (
    <Grid item xs={12} sm={6} md={4}>
      <CardWrapper>
        <Box
          sx={{
            position: 'relative',
            zIndex: '2'
          }}
        >
          <Box
            px={2}
            pt={2}
            display="flex"
            alignItems="flex-start"
            justifyContent="space-between"
          >
            {userRoleMapping[userCard.role].chip}
          </Box>
          <Box p={2} display="flex" alignItems="flex-start">
            <Avatar
              sx={{
                width: 50,
                height: 50,
                mr: 2
              }}
              src={userCard.avatarPath}
            />
            <Box>
              <Box>
                <Typography component="span" variant="h5">
                  {`${userCard.name} ${userCard.surname}`}
                </Typography>
              </Box>
              <Typography
                sx={{
                  pt: 0.3
                }}
                variant="subtitle2"
              >
                {userCard.position}
              </Typography>
              <Typography
                sx={{
                  pt: 1
                }}
                variant="h6"
              >
                {userCard.login}
              </Typography>
            </Box>
          </Box>
          <Divider />
          <Box
            pl={2}
            py={1}
            pr={1}
            display="flex"
            alignItems="center"
            justifyContent="flex-end"
          >
            <Button
              sx={{
                m: 1
              }}
              size="small"
              onClick={() =>
                createChat({
                  personIds: [userCard.id, user?.id],
                  name: 'chat'
                })
              }
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Cоздать новый чат
            </Button>
          </Box>
        </Box>
      </CardWrapper>
    </Grid>
  );
}

const CardWrapper = styled(Card)(
  ({ theme }) => `

  position: relative;
  overflow: visible;

  &::after {
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    border-radius: inherit;
    z-index: 1;
    transition: ${theme.transitions.create(['box-shadow'])};
  }
      
    &.Mui-selected::after {
      box-shadow: 0 0 0 3px ${theme.colors.primary.main};
    }
  `
);
