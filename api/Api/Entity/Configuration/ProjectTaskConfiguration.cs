﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Api.Entity.Configuration
{
    public class ProjectTaskConfiguration : IEntityTypeConfiguration<ProjectTask>
    {
        public void Configure(EntityTypeBuilder<ProjectTask> builder)
        {
            builder.HasKey(x => x.Id);
            
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.Status).IsRequired();
            builder.Property(x => x.ProjectId).IsRequired();

            builder.HasOne(x => x.Assigned)
                .WithMany(x => x.ProjectTasks)
                .HasForeignKey(x => x.AssignedId);

            builder.HasOne(x => x.Project)
                .WithMany(x => x.ProjectTasks)
                .HasForeignKey(x => x.ProjectId);
        }
    }
}