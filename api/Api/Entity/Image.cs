﻿namespace Api.Entity
{
    public class Image
    {
        public int Id { get; set; }
        public string Path { get; set; }
    }
}