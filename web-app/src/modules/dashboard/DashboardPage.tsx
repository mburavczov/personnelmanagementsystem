import { Footer, StandartPageHeader } from '@/components';
import { useAuth } from '@/hooks';
import { useStatisticsQuery } from '@/api/queries/use-statistics.query';
import {
  Grid,
  Typography,
  ListItemAvatar,
  Card,
  ListItemText,
  ListItem,
  CardContent,
  styled,
  Avatar,
  Divider,
  Box
} from '@mui/material';
import { taskStatusesMapping } from '@/utils';
import { TaskStatuses } from '@/models/enums';
import FiberNewIcon from '@mui/icons-material/FiberNew';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import HourglassTopIcon from '@mui/icons-material/HourglassTop';
import AutorenewIcon from '@mui/icons-material/Autorenew';

export function DashboardPage() {
  const { user } = useAuth();
  const { data } = useStatisticsQuery();

  if (!data) return null;

  let totalTasks: number = 0;
  let newTasks: number = 0;
  let inprogressTasks: number = 0;
  let waitTasks: number = 0;
  let doneTasks: number = 0;
  let spentTime: number = 0;

  data.items.forEach((item) => {
    totalTasks += item.totalTasks;
    newTasks += item.newTasks;
    inprogressTasks += item.inprogressTasks;
    waitTasks += item.waitTasks;
    doneTasks += item.doneTasks;
    spentTime += item.spentTime;
  });

  const items = [
    {
      title: 'Новые задачи',
      desc: 'Кол-во новых задач',
      color: taskStatusesMapping[TaskStatuses.NEW].color,
      count: newTasks,
      icon: FiberNewIcon
    },
    {
      title: 'Задачи в процессе',
      desc: 'Кол-во задач в процессе',
      color: taskStatusesMapping[TaskStatuses.IN_PROGRESS].color,
      count: inprogressTasks,
      icon: AutorenewIcon
    },
    {
      title: 'Задачи в ожидании',
      desc: 'Кол-во задач в ожидании',
      color: taskStatusesMapping[TaskStatuses.WAIT].color,
      count: waitTasks,
      icon: HourglassTopIcon
    },
    {
      title: 'Выполенные задачи',
      desc: 'Кол-во выполненных задач',
      color: taskStatusesMapping[TaskStatuses.DONE].color,
      count: doneTasks,
      icon: CheckCircleIcon
    }
  ];

  return (
    <>
      <StandartPageHeader
        title={`Добро пожаловать, ${user?.name}!`}
        caption="В нашей системе вы можете..."
      />

      <Grid
        sx={{ px: 4 }}
        container
        direction="row"
        justifyContent="center"
        alignItems="stretch"
        spacing={3}
      >
        <Grid item lg={8} md={6} xs={12}>
          <Grid
            container
            spacing={3}
            direction="row"
            justifyContent="center"
            alignItems="stretch"
          >
            {items.map((item) => {
              const Icon = item.icon;

              return (
                <Grid item sm={6} xs={12} key={item.color}>
                  <Card>
                    <CardContentWrapper>
                      <Typography variant="overline" color="text.primary">
                        {item.title}
                      </Typography>

                      <ListItem
                        disableGutters
                        sx={{
                          my: 1
                        }}
                        component="div"
                      >
                        <ListItemAvatar>
                          <AvatarCustom
                            variant="rounded"
                            baseColor={item.color}
                          >
                            <Icon fontSize="large" />
                          </AvatarCustom>
                        </ListItemAvatar>

                        <ListItemText
                          primary={item.count}
                          primaryTypographyProps={{
                            variant: 'h1',
                            sx: {
                              ml: 2
                            },
                            noWrap: true
                          }}
                        />
                      </ListItem>
                      <ListItem
                        disableGutters
                        sx={{
                          mt: 0.5,
                          mb: 1.5
                        }}
                        component="div"
                      >
                        <ListItemText
                          primary={<>{item.desc}</>}
                          primaryTypographyProps={{
                            variant: 'body2',
                            noWrap: true
                          }}
                        />
                      </ListItem>
                    </CardContentWrapper>
                  </Card>
                </Grid>
              );
            })}
          </Grid>
        </Grid>
        <Grid item lg={4} md={6} xs={12}>
          <Card
            sx={{
              py: 2,
              display: 'flex',
              alignItems: 'center',
              height: '100%'
            }}
          >
            <Box
              sx={{
                width: '100%'
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Typography
                  variant="h1"
                  sx={{
                    fontSize: 40
                  }}
                >
                  {totalTasks}
                </Typography>
                <Divider
                  sx={{
                    my: 2,
                    height: 4,
                    width: '20%'
                  }}
                />
                <Typography
                  variant="h3"
                  fontWeight="bold"
                  sx={{
                    fontSize: 14,
                    textTransform: 'uppercase'
                  }}
                >
                  задач
                </Typography>
              </Box>

              <Divider
                sx={{
                  mx: 3,
                  my: 4
                }}
              />

              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Typography
                  variant="h1"
                  sx={{
                    fontSize: 40
                  }}
                >
                  {spentTime}
                </Typography>
                <Divider
                  sx={{
                    my: 2,
                    height: 4,
                    width: '20%'
                  }}
                />
                <Typography
                  variant="h3"
                  fontWeight="bold"
                  sx={{
                    fontSize: 14,
                    textTransform: 'uppercase'
                  }}
                >
                  отработанных часов
                </Typography>
              </Box>
            </Box>
          </Card>
        </Grid>
      </Grid>

      <Footer />
    </>
  );
}

const CardContentWrapper = styled(CardContent)(
  ({ theme }) => `
     padding: ${theme.spacing(2.5, 3, 3)};
  
     &:last-child {
     padding-bottom: 0;
     }
`
);

const AvatarCustom = styled(Avatar)<{ baseColor: string }>(
  ({ theme, baseColor }) => `
      background-color: ${theme.colors[baseColor].main};
      color: ${theme.palette.primary.contrastText};
      width: ${theme.spacing(8)};
      height: ${theme.spacing(8)};
      box-shadow: ${theme.colors.shadows[baseColor]};
`
);
