﻿using System;
using System.Collections.Generic;
using Api.ApiModel.Response.Profile;
using Api.Entity;

namespace Api.ApiModel.Response.ProjectTask
{
    public class TaskItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
        public int SpentTimes { get; set; }
        
        public PersonItem Assigned { get; set; }
        public List<TimeLogItem> TimeLogs { get; set; }
    }
}