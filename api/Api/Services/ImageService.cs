﻿using System;
using System.IO;
using System.Threading.Tasks;
using Api.Entity;
using Api.Repository;

namespace Api.Services
{
    public class ImageService
    {
        private readonly CdnService _cdnService;
        private readonly ImageRepository _imageRepository;

        public ImageService(CdnService cdnService, ImageRepository imageRepository)
        {
            _cdnService = cdnService;
            _imageRepository = imageRepository;
        }

        public async Task<string> Upload(Stream fs)
        {
            var filePath = $"{Guid.NewGuid().ToString()}.jpg";
            var bytes = ReadFully(fs);
            await _cdnService.Upload(bytes, filePath);

            _imageRepository.Add(new Image { Path = filePath });

            return filePath;
        }
        
        private static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
    }
}