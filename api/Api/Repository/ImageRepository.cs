﻿using Api.Entity;

namespace Api.Repository
{
    public class ImageRepository : BaseRepository<Image>
    {
        public ImageRepository(MainContext context) : base(context)
        {
        }
    }
}