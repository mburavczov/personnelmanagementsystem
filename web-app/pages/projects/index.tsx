import Head from 'next/head';
import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';
import { ProjectsPage } from '@/modules/projects';

function Projects() {
  return (
    <>
      <Head>
        <title>Мои проекты</title>
      </Head>
      <ProjectsPage />
    </>
  );
}

Projects.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default Projects;
