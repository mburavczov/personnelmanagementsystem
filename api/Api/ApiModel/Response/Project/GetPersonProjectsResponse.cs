﻿using System.Collections.Generic;

namespace Api.ApiModel.Response.Project
{
    public class GetPersonProjectsResponse
    {
        public List<ProjectItem> Projects { get; set; }
    }
}