import Head from 'next/head';
import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';
import { UsersPage } from '@/modules/users';

function Users() {
  return (
    <>
      <Head>
        <title>Пользователи</title>
      </Head>
      <UsersPage />
    </>
  );
}

Users.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default Users;
