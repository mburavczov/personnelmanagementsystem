import { DialogPopup } from '@/components';
import {
  Autocomplete,
  Button,
  CircularProgress,
  TextField,
  Typography
} from '@mui/material';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useCreateProjectMutation, usePersonsQuery } from '@/api';
import { UserRoles } from '@/models/enums';
import { useQueryClient } from 'react-query';

interface CreateProjectPopupProps {
  open: boolean;
  toggleOpen: () => void;
}

export function CreateProjectPopup({
  open,
  toggleOpen
}: CreateProjectPopupProps) {
  const queryClient = useQueryClient();
  const { data: managerUsers = [] } = usePersonsQuery({
    role: UserRoles.MANAGER
  });
  const { data: customerUsers = [] } = usePersonsQuery({
    role: UserRoles.CUSTOMER
  });

  const createProject = useCreateProjectMutation({
    onSuccess: () => {
      toggleOpen();
      queryClient.invalidateQueries('projects');
    }
  });
  const formik = useFormik({
    initialValues: {
      name: '',
      description: '',
      managerId: '',
      customerId: ''
    },
    validationSchema: Yup.object({
      name: Yup.string().required('Введите название проекта.'),
      description: Yup.string().required('Введите описание проекта.'),
      managerId: Yup.number().required('Выберите менеджера проекта.'),
      customerId: Yup.number().required('Выберите заказчика проекта.')
    }),
    onSubmit: async (values): Promise<void> => {
      await createProject(values);
    }
  });

  return (
    <>
      <DialogPopup
        header={
          <Typography variant="h4" gutterBottom>
            Создание проекта
          </Typography>
        }
        actions={
          <>
            <Button color="secondary" onClick={toggleOpen}>
              Отмена
            </Button>
            <Button
              onClick={() => formik.handleSubmit()}
              type="submit"
              startIcon={
                formik.isSubmitting ? <CircularProgress size="1rem" /> : null
              }
              disabled={formik.isSubmitting}
              variant="contained"
            >
              Создать
            </Button>
          </>
        }
        open={open}
        onClose={toggleOpen}
      >
        <TextField
          error={Boolean(formik.touched.name && formik.errors.name)}
          fullWidth
          margin="normal"
          helperText={formik.touched.name && formik.errors.name}
          label={'Название проекта'}
          name="name"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.name}
          variant="outlined"
        />
        <TextField
          error={Boolean(
            formik.touched.description && formik.errors.description
          )}
          fullWidth
          margin="normal"
          helperText={formik.touched.description && formik.errors.description}
          label={'Описание проекта'}
          name="description"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.description}
          variant="outlined"
          multiline
        />
        <Autocomplete
          options={managerUsers.map((user) => ({
            label: `${user.name} ${user.surname}`,
            value: user.id
          }))}
          onChange={(_, selectedOption) => {
            formik.setFieldValue('managerId', selectedOption?.value as number);
          }}
          renderInput={(params) => (
            <TextField
              fullWidth
              margin="normal"
              {...params}
              label={'Менеджер проекта'}
              error={Boolean(
                formik.touched.managerId && formik.errors.managerId
              )}
              helperText={formik.touched.managerId && formik.errors.managerId}
            />
          )}
        />
        <Autocomplete
          options={customerUsers.map((user) => ({
            label: `${user.name} ${user.surname}`,
            value: user.id
          }))}
          onChange={(_, selectedOption) => {
            formik.setFieldValue('customerId', selectedOption?.value as number);
          }}
          renderInput={(params) => (
            <TextField
              fullWidth
              margin="normal"
              {...params}
              label={'Заказчик проекта'}
              error={Boolean(
                formik.touched.customerId && formik.errors.customerId
              )}
              helperText={formik.touched.customerId && formik.errors.customerId}
            />
          )}
        />
      </DialogPopup>
    </>
  );
}
