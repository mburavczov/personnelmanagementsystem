import Head from 'next/head';
import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';
import { ProjectDetailPage } from '@/modules/projects';

function Project() {
  return (
    <>
      <Head>
        <title>Проект</title>
      </Head>
      <ProjectDetailPage />
    </>
  );
}

Project.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default Project;
