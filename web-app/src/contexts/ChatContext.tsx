import {
  useState,
  ReactNode,
  createContext,
  Dispatch,
  SetStateAction
} from 'react';

type ChatContext = {
  chatId: number;
  setChatId: Dispatch<SetStateAction<number>>;
  selectUser: number;
  setSelectUser: Dispatch<SetStateAction<number>>;
};

// eslint-disable-next-line @typescript-eslint/no-redeclare
export const ChatContext = createContext<ChatContext>({} as ChatContext);

type Props = {
  children: ReactNode;
};

export function ChatProvider({ children }: Props) {
  const [chatId, setChatId] = useState(0);
  const [selectUser, setSelectUser] = useState(0);

  return (
    <ChatContext.Provider
      value={{ chatId, setChatId, selectUser, setSelectUser }}
    >
      {children}
    </ChatContext.Provider>
  );
}
