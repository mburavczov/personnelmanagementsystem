import { useQuery } from 'react-query';
import { client } from '@/api';

export interface ChatMessagesRequest {
  chatId: number;
}

export type ChatMessagesResponse = {
  messages: {
    text: string;
    senderId: number;
    senderName: string;
    createdAt: Date;
  }[];
};

export function useChatMessages(params: ChatMessagesRequest) {
  return useQuery<ChatMessagesResponse>(
    ['chatMessages', ...Object.values(params)],
    async () => {
      try {
        const dto = await client.get('Chat/ChatMessages', {
          params
        });
        return dto.data;
      } catch {
        console.log('fatal error');
      }
    },
    { refetchInterval: 3000 }
  );
}
