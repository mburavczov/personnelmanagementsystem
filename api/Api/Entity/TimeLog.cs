﻿using System;

namespace Api.Entity
{
    public class TimeLog
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public int PersonId { get; set; }
        public int Minutes { get; set; }
        public string Comment { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        
        public virtual ProjectTask ProjectTask { get; set; }
        public virtual Person Person { get; set; }
    }
}