﻿using System;
using System.Collections.Generic;

namespace Api.Entity
{
    public class ProjectTask
    {
        public ProjectTask()
        {
            TimeLogs = new List<TimeLog>();
        }
        
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? AssignedId { get; set; }
        public int ProjectId { get; set; }
        public ProjectTaskStatus Status { get; set; }
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
        
        public virtual Person Assigned { get; set; }
        public virtual Project Project { get; set; }
        public virtual ICollection<TimeLog> TimeLogs { get; set; }
    }
}