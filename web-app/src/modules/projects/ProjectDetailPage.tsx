import { Footer, Link, StandartPageHeader } from '@/components';
import { useAuth, useBooleanState, useRouterQueryId } from '@/hooks';
import {
  Button,
  Card,
  CardHeader,
  Divider,
  Grid,
  Typography,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  Box,
  styled,
  Tooltip,
  Badge,
  useTheme
} from '@mui/material';
import {
  AddMemberToProjectPopup,
  UpdateProjectPopup
} from '@/modules/projects/popups';
import { UserRoles } from '@/models/enums';
import { useProjectDetailQuery } from '@/api';
import EditIcon from '@mui/icons-material/Edit';
import React from 'react';
import { TasksProjects } from '@/modules/projects/project-detail/TasksProjects';
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import AssignmentIcon from '@mui/icons-material/Assignment';

export function ProjectDetailPage() {
  const theme = useTheme();
  const { user } = useAuth();
  const projectId = useRouterQueryId();
  const { data: project } = useProjectDetailQuery({ projectId });
  const [addMemberOpen, toggleAddMemberOpen] = useBooleanState();
  const [updateProjectOpen, toggleUpdateProjectOpen] = useBooleanState();

  const isActionAddMemberToProject =
    user?.role === UserRoles.ADMIN || user?.role === UserRoles.MANAGER;
  const isActionUpdateProject = user?.role === UserRoles.ADMIN;

  if (!project) {
    return null;
  }

  return (
    <>
      <StandartPageHeader
        title={`Проект "${project.name}"`}
        caption={`Описание: ${project.description}`}
        action={
          isActionUpdateProject && (
            <Button
              sx={{
                m: 1
              }}
              onClick={() => toggleUpdateProjectOpen()}
              variant="contained"
              startIcon={<EditIcon fontSize="small" />}
            >
              Редактировать
            </Button>
          )
        }
      />

      <Grid
        sx={{
          px: 4
        }}
        container
        direction="row"
        justifyContent="center"
        alignItems="stretch"
        spacing={4}
      >
        <Grid item xs={12}>
          <TasksProjects />
        </Grid>
      </Grid>

      <Grid
        sx={{
          p: 4
        }}
        container
        direction="row"
        justifyContent="center"
        alignItems="stretch"
        spacing={4}
      >
        <Grid item md={6} xs={12}>
          <Card variant="outlined">
            <List>
              <ListItem
                sx={{
                  py: 2.5
                }}
                alignItems="flex-start"
              >
                <ListItemAvatar>
                  <Badge
                    overlap="rectangular"
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'right'
                    }}
                    badgeContent={
                      <Tooltip arrow placement="top" title={'Владелец проекта'}>
                        <DotLegend
                          style={{ background: `${theme.colors.info.main}` }}
                        />
                      </Tooltip>
                    }
                  >
                    <AvatarWrapper
                      variant="rounded"
                      alt={`${project.customer.name} ${project.customer.surname}`}
                      src={project.customer.avatarPath}
                    />
                  </Badge>
                </ListItemAvatar>
                <ListItemText
                  sx={{
                    p: 2
                  }}
                  primary={
                    <Typography variant="h4" gutterBottom>
                      {project.customer.name} {project.customer.surname}
                    </Typography>
                  }
                  secondary="Владелец проекта"
                  secondaryTypographyProps={{ variant: 'subtitle2' }}
                />
                <Box display="flex" alignItems="center" sx={{ my: 'auto' }}>
                  <Link
                    href={`mailto:${project.customer.login}`}
                    color="text.primary"
                    variant="subtitle1"
                  >
                    {project.customer.login}
                  </Link>
                </Box>
              </ListItem>
              <Divider variant="inset" component="li" />
              <ListItem
                sx={{
                  py: 2.5
                }}
                alignItems="flex-start"
              >
                <ListItemAvatar>
                  <Badge
                    overlap="rectangular"
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'right'
                    }}
                    badgeContent={
                      <Tooltip arrow placement="top" title={'Менеджер проекта'}>
                        <DotLegend
                          style={{ background: `${theme.colors.warning.main}` }}
                        />
                      </Tooltip>
                    }
                  >
                    <AvatarWrapper
                      variant="rounded"
                      alt={`${project.manager.name} ${project.manager.surname}`}
                      src={project.manager.avatarPath}
                    />
                  </Badge>
                </ListItemAvatar>
                <ListItemText
                  sx={{
                    p: 2
                  }}
                  primary={
                    <Typography variant="h4" gutterBottom>
                      {project.manager.name} {project.manager.surname}
                    </Typography>
                  }
                  secondary="Менеджер проекта"
                  secondaryTypographyProps={{ variant: 'subtitle2' }}
                />
                <Box display="flex" alignItems="center" sx={{ my: 'auto' }}>
                  <Link
                    href={`mailto:${project.manager.login}`}
                    color="text.primary"
                    variant="subtitle1"
                  >
                    {project.manager.login}
                  </Link>
                </Box>
              </ListItem>
            </List>
          </Card>
          <Card
            sx={{
              mt: 4
            }}
            variant="outlined"
          >
            <CardHeader
              sx={{
                p: 3
              }}
              disableTypography
              title={
                <Box display="flex" alignItems="center">
                  <Typography
                    sx={{
                      display: 'flex',
                      alignItems: 'center',
                      opacity: 0.4
                    }}
                    color="text.secondary"
                  >
                    <AssignmentIcon
                      sx={{
                        fontSize: 51
                      }}
                    />
                  </Typography>
                  <Box ml={1}>
                    <Typography noWrap gutterBottom variant="subtitle2">
                      Задачи
                    </Typography>
                    <Typography color="primary" variant="h4">
                      {project.tasks.length}
                    </Typography>
                  </Box>
                </Box>
              }
              action={
                <Box py={1}>
                  <Button
                    component={Link}
                    href={`/projects/${project.id}/tasks`}
                  >
                    Задачи проекта
                  </Button>
                </Box>
              }
            />
          </Card>
        </Grid>
        <Grid item md={6} xs={12}>
          <Card variant="outlined">
            <CardHeader
              sx={{
                p: 3
              }}
              disableTypography
              title={
                <Typography
                  variant="h4"
                  sx={{
                    fontSize: 16
                  }}
                >
                  Сотрудники
                </Typography>
              }
              action={
                isActionAddMemberToProject && (
                  <Button
                    sx={{
                      m: 1
                    }}
                    size="small"
                    onClick={() => toggleAddMemberOpen()}
                    variant="contained"
                    startIcon={<AddTwoToneIcon fontSize="small" />}
                  >
                    Добавить
                  </Button>
                )
              }
            />
            <List
              sx={{
                py: 0
              }}
            >
              {project.persons.map((person) => (
                <>
                  <Divider />
                  <ListItem
                    sx={{
                      px: 2,
                      py: 1.95
                    }}
                  >
                    <ListItemAvatar
                      sx={{
                        mr: 2,
                        minWidth: 0
                      }}
                    >
                      <Avatar src={person.avatarPath} />
                    </ListItemAvatar>
                    <ListItemText
                      primary={
                        <Typography variant="h5" color="text.primary" noWrap>
                          {person.name} {person.surname}
                        </Typography>
                      }
                      secondary={
                        <Typography variant="subtitle2" noWrap>
                          {person.position}
                        </Typography>
                      }
                    />
                    <Box display="flex" alignItems="center">
                      <Link
                        href={`mailto:${person.login}`}
                        color="text.primary"
                        variant="subtitle1"
                      >
                        {person.login}
                      </Link>
                    </Box>
                  </ListItem>
                </>
              ))}
            </List>
          </Card>
        </Grid>
      </Grid>

      <Footer />

      {isActionAddMemberToProject && (
        <AddMemberToProjectPopup
          projectId={projectId}
          open={addMemberOpen}
          toggleOpen={toggleAddMemberOpen}
        />
      )}
      {isActionUpdateProject && (
        <UpdateProjectPopup
          projectId={projectId}
          open={updateProjectOpen}
          toggleOpen={toggleUpdateProjectOpen}
        />
      )}
    </>
  );
}

const DotLegend = styled('span')(
  ({ theme }) => `
    border-radius: 22px;
    width: ${theme.spacing(2)};
    height: ${theme.spacing(2)};
    display: inline-block;
    margin-right: ${theme.spacing(0.5)};
    border: ${theme.colors.alpha.white[100]} solid 2px;
`
);

const AvatarWrapper = styled(Avatar)(
  ({ theme }) => `
    width: ${theme.spacing(9)};
    height: ${theme.spacing(9)};
`
);
