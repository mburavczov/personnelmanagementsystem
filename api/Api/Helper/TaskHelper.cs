﻿using System.Linq;
using Api.ApiModel.Response.ProjectTask;
using Api.Entity;

namespace Api.Helper
{
    public class TaskHelper
    {
        public static TaskItem MapProjectTask(ProjectTask task)
        {
            return new TaskItem
            {
                Id = task.Id,
                Name = task.Name,
                Description = task.Description,
                Status = task.Status.Name(),
                UpdatedAt = task.UpdatedAt,
                SpentTimes = task.TimeLogs.Sum(x => x.Minutes),
                Assigned = PersonHelper.MapPerson(task.Assigned),
                TimeLogs = task.TimeLogs.Select(MapTimeLog).ToList()
            };
        }

        public static TimeLogItem MapTimeLog(TimeLog data)
        {
            return new TimeLogItem
            {
                Id = data.Id,
                Minutes = data.Minutes,
                Comment = data.Comment,
                CreatedAt = data.CreatedAt,
                Person = PersonHelper.MapPerson(data.Person)
            };
        }
    }
}