import { UserRoles } from '@/models/enums';
import { Label } from '@/components';

export const userRoleMapping = {
  [UserRoles.ADMIN]: {
    label: 'Администратор',
    chip: <Label color="error">Администратор</Label>
  },
  [UserRoles.MANAGER]: {
    label: 'Менеджер',
    chip: <Label color="warning">Менеджер</Label>
  },
  [UserRoles.EMPLOYEE]: {
    label: 'Сотрудник',
    chip: <Label color="success">Сотрудник</Label>
  },
  [UserRoles.CUSTOMER]: {
    label: 'Клиент',
    chip: <Label color="info">Заказчик</Label>
  }
};
