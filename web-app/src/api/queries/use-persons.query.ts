import { useQuery } from 'react-query';
import { client } from '@/api';
import { User } from '@/models';
import { UserRoles } from '@/models/enums';

export interface GetPersonsRequest {
  role: UserRoles;
}

export type GetPersonsResponse = User[];

export function usePersonsQuery(params: GetPersonsRequest) {
  return useQuery<GetPersonsResponse>(
    ['persons', ...Object.values(params)],
    async () => {
      try {
        const dto = await client.get('Profile/Persons', {
          params
        });
        return dto.data;
      } catch {
        console.log('fatal error');
      }
    },
    {}
  );
}
