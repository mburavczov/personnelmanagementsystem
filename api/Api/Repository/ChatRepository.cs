﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Api.Entity;

namespace Api.Repository
{
    public class ChatRepository : BaseRepository<Chat>
    {
        public ChatRepository(MainContext context) : base(context)
        {
        }

        public void Update(Chat chat)
        {
            DbSet.Update(chat);
            Context.SaveChanges();
        }

        public Chat GetById(int id)
        {
            return DbSet.FirstOrDefault(x => x.Id == id);
        }

        public List<Chat> GetByPersonId(int personId)
        {
            return DbSet
                .Include(x => x.Persons)
                .Where(x => x.Persons.Any(y => y.Id == personId))
                .OrderByDescending(x => x.CreatedAt)
                .ToList();
        }

        public bool HasByIdAndSenderId(int id, int senderId)
        {
            return DbSet.Include(x => x.Persons)
                .Any(x => x.Id == id && x.Persons.Any(y => y.Id == senderId));
        }
    }
}