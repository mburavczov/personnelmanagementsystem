import { Box, Card, styled } from '@mui/material';
import { useContext } from 'react';
import { ChatContext } from '@/contexts/ChatContext';
import { useChatMessages } from '@/api';
import { useAuth } from '@/hooks';

export function ChatContent() {
  const { chatId } = useContext(ChatContext);
  const { user } = useAuth();
  const { data } = useChatMessages({ chatId });

  if (!data) return null;

  return (
    <BoxChat p={3}>
      {data.messages.map((message, index) => (
        <BoxChatMessage
          key={index}
          display="flex"
          alignItems="flex-start"
          justifyContent="flex-end"
          py={3}
        >
          <Box
            display="flex"
            alignItems="flex-end"
            flexDirection="column"
            justifyContent="flex-end"
            mr={2}
          >
            {message.senderId === user?.id ? (
              <CardWrapperPrimary>{message.text}</CardWrapperPrimary>
            ) : (
              <CardWrapperSecondary>{message.text}</CardWrapperSecondary>
            )}
          </Box>
        </BoxChatMessage>
      ))}
    </BoxChat>
  );
}

const BoxChat = styled(Box)`
  overflow-y: scroll;
  direction: rtl;
  transform: rotate(180deg);
`;

const BoxChatMessage = styled(Box)`
  direction: ltr;
  transform: rotate(180deg);
`;

const CardWrapperPrimary = styled(Card)(
  ({ theme }) => `
      background: ${theme.colors.primary.main};
      color: ${theme.palette.primary.contrastText};
      padding: ${theme.spacing(2)};
      border-radius: ${theme.general.borderRadiusXl};
      border-top-right-radius: ${theme.general.borderRadius};
      max-width: 380px;
      display: inline-flex;
`
);

const CardWrapperSecondary = styled(Card)(
  ({ theme }) => `
      background: ${theme.colors.alpha.black[10]};
      color: ${theme.colors.alpha.black[100]};
      padding: ${theme.spacing(2)};
      border-radius: ${theme.general.borderRadiusXl};
      border-top-left-radius: ${theme.general.borderRadius};
      max-width: 380px;
      display: inline-flex;
`
);
