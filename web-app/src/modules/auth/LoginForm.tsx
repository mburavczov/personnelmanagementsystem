import {
  Button,
  CircularProgress,
  FormHelperText,
  TextField
} from '@mui/material';
import { useAuth, useRefMounted } from '@/hooks';
import * as Yup from 'yup';
import { useFormik } from 'formik';
import { useRouter } from 'next/router';

export function LoginForm() {
  const { login } = useAuth();
  const isMountedRef = useRefMounted();
  const router = useRouter();

  const formik = useFormik({
    initialValues: {
      login: 'string@string.ru',
      pass: 'string',
      submit: null
    },
    validationSchema: Yup.object({
      login: Yup.string()
        .email('Введен некорректный Email.')
        .max(255)
        .required('Не введен Email.'),
      pass: Yup.string().max(255).required('Не введен пароль.'),
      submit: null
    }),
    onSubmit: async (values, helpers): Promise<void> => {
      try {
        await login(values);

        if (isMountedRef()) {
          const backTo = (router.query.backTo as string) || '/dashboard';
          router.push(backTo);
        }
      } catch (err) {
        console.error(err);
        if (isMountedRef()) {
          helpers.setStatus({ success: false });
          helpers.setErrors({
            submit: 'Неверный логин или пароль.'
          });
          helpers.setSubmitting(false);
        }
      }
    }
  });

  return (
    <form noValidate onSubmit={formik.handleSubmit}>
      <TextField
        error={Boolean(formik.touched.login && formik.errors.login)}
        fullWidth
        margin="normal"
        autoFocus
        helperText={formik.touched.login && formik.errors.login}
        label={'Email'}
        name="login"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        type="email"
        value={formik.values.login}
        variant="outlined"
      />
      <TextField
        error={Boolean(formik.touched.pass && formik.errors.pass)}
        fullWidth
        margin="normal"
        helperText={formik.touched.pass && formik.errors.pass}
        label={'Пароль'}
        name="pass"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        type="password"
        value={formik.values.pass}
        variant="outlined"
      />

      {Boolean(formik.touched.submit && formik.errors.submit) && (
        <FormHelperText error>{formik.errors.submit}</FormHelperText>
      )}

      <Button
        sx={{
          mt: 3
        }}
        color="primary"
        startIcon={
          formik.isSubmitting ? <CircularProgress size="1rem" /> : null
        }
        disabled={formik.isSubmitting}
        type="submit"
        fullWidth
        size="large"
        variant="contained"
      >
        Войти
      </Button>
    </form>
  );
}
