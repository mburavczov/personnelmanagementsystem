import { Task } from '@/models';
import {
  Avatar,
  Box,
  Button,
  Card,
  CardHeader,
  Divider,
  IconButton,
  Typography
} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import { Link } from '@/components';
import React from 'react';
import { getHoursFromMinute } from '@/utils';
import { useAuth, useBooleanState } from '@/hooks';
import { UserRoles } from '@/models/enums';
import { UpdateTaskPopup } from '@/modules/projects/popups/UpdateTaskPopup';

interface TaskCardProps {
  projectId: number;
  task: Task;
}

export function TaskCard({ projectId, task }: TaskCardProps) {
  const { user } = useAuth();
  const { minutes, hours } = getHoursFromMinute(task.spentTimes);
  const [updateTaskOpen, toggleUpdateTaskOpen] = useBooleanState();

  const userRole = user?.role;
  const isUpdateTask =
    userRole === UserRoles.ADMIN ||
    userRole === UserRoles.MANAGER ||
    task.assigned.id === user?.id;

  return (
    <>
      <Card
        key={task.id}
        sx={{
          m: 2,
          p: 1
        }}
      >
        <CardHeader
          title={
            <Typography variant="h4" gutterBottom>
              {task.name}
            </Typography>
          }
          action={
            isUpdateTask && (
              <IconButton
                color="primary"
                sx={{
                  p: 0.5
                }}
                onClick={toggleUpdateTaskOpen}
              >
                <EditIcon fontSize="small" />
              </IconButton>
            )
          }
        />
        <Divider />

        <Box py={1} px={2} display="flex" alignItems="flex-start">
          <Avatar
            sx={{
              width: 50,
              height: 50,
              mr: 2
            }}
            src={task.assigned.avatarPath}
          />
          <Box>
            <Box>
              <Typography component="span" variant="h5">
                {`${task.assigned.name} ${task.assigned.surname}`}
              </Typography>
            </Box>
            <Typography
              sx={{
                pt: 0.3
              }}
              variant="subtitle2"
            >
              {task.assigned.position}
            </Typography>
          </Box>
        </Box>
        <Divider />
        <Box
          pt={1}
          display="flex"
          justifyContent="space-between"
          alignItems="center"
        >
          <Typography variant="h6" pl={1}>
            Время:{' '}
            <b>
              {hours}:{minutes}
            </b>
          </Typography>
          <Button
            component={Link}
            href={`/projects/${projectId}/tasks/${task.id}`}
          >
            Подробнее
          </Button>
        </Box>
      </Card>

      <UpdateTaskPopup
        open={updateTaskOpen}
        toggleOpen={toggleUpdateTaskOpen}
        task={task}
        projectId={projectId}
      />
    </>
  );
}
