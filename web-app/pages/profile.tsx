import Head from 'next/head';
import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';
import { ProfilePage } from '@/modules/profile';

function Profile() {
  return (
    <>
      <Head>
        <title>Мой профиль</title>
      </Head>
      <ProfilePage />
    </>
  );
}

Profile.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default Profile;
