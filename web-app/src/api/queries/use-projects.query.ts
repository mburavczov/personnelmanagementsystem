import { Project } from '@/models';
import { useQuery } from 'react-query';
import { client } from '@/api';

export interface GetProjectsRequest {
  personId?: number;
}

export interface GetProjectsResponse {
  projects: Project[];
}

export function useProjectsQuery(params: GetProjectsRequest) {
  return useQuery<GetProjectsResponse>(
    ['projects', ...Object.values(params)],
    async () => {
      try {
        const dto = await client.get('Project/GetPersonProjects', {
          params
        });
        return dto.data;
      } catch {
        console.log('fatal error');
      }
    },
    {}
  );
}
