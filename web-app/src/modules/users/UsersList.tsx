import { usePersonsQuery } from '@/api';
import { SyntheticEvent, useState } from 'react';
import { Box, Grid, styled, Tab, Tabs } from '@mui/material';
import { UserRoles } from '@/models/enums';
import { UserListCard } from '@/modules/users/UserListCard';

export function UsersList() {
  const [role, setRole] = useState(null);
  const { data: users = [] } = usePersonsQuery({ role: undefined });

  const tabs = [
    {
      value: null,
      label: 'Все пользователи'
    },
    {
      value: UserRoles.ADMIN,
      label: 'Администраторы'
    },
    {
      value: UserRoles.MANAGER,
      label: 'Менеджеры'
    },
    {
      value: UserRoles.EMPLOYEE,
      label: 'Сотрудники'
    },
    {
      value: UserRoles.CUSTOMER,
      label: 'Заказчики'
    }
  ];

  const handleTabsChange = (
    _event: SyntheticEvent,
    tabsValue: UserRoles | undefined
  ) => {
    setRole(tabsValue);
  };

  return (
    <>
      <Box
        display="flex"
        alignItems="center"
        flexDirection={{ xs: 'column', sm: 'row' }}
        justifyContent={{ xs: 'center', sm: 'space-between' }}
        pb={3}
      >
        <TabsWrapper
          onChange={handleTabsChange}
          scrollButtons="auto"
          textColor="secondary"
          value={role}
          variant="scrollable"
        >
          {tabs.map((tab) => (
            <Tab key={tab.value} value={tab.value} label={tab.label} />
          ))}
        </TabsWrapper>
      </Box>

      <Grid container spacing={3}>
        {users
          .filter((user) => user.role === role || role === null)
          .map((user) => (
            <UserListCard key={user.id} userCard={user} />
          ))}
      </Grid>
    </>
  );
}

const TabsWrapper = styled(Tabs)(
  ({ theme }) => `
    @media (max-width: ${theme.breakpoints.values.md}px) {
      .MuiTabs-scrollableX {
        overflow-x: auto !important;
      }

      .MuiTabs-indicator {
          box-shadow: none;
      }
    }
    `
);
