﻿namespace Api.Entity
{
    public enum PersonRole
    {
        Admin = 1,
        Manager = 2,
        Employee = 3,
        Customer = 4,
    }

    public static class RoleExtension
    {
        public static string Name(this PersonRole personRole)
        {
            return personRole switch
            {
                PersonRole.Admin => "admin",
                PersonRole.Manager => "manager",
                PersonRole.Employee => "employee",
                PersonRole.Customer => "customer",
                _ => ""
            };
        }
    }
}