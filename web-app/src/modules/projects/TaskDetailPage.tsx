import { Footer, StandartPageHeader } from '@/components';
import { useAuth, useBooleanState, useRouterQueryId } from '@/hooks';
import { useProjectDetailQuery, useTaskDetailQuery } from '@/api';
import { getHoursFromMinute, taskStatusesMapping } from '@/utils';
import { UserRoles } from '@/models/enums';
import {
  Box,
  Button,
  Card,
  Grid,
  styled,
  Tooltip,
  Typography,
  useTheme
} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import React from 'react';
import { UpdateTaskPopup } from '@/modules/projects/popups/UpdateTaskPopup';
import { TimeLogs } from '@/modules/projects/task-detail/TimeLogs';

export function TaskDetailPage() {
  const theme = useTheme();
  const { user } = useAuth();
  const [updateTaskOpen, toggleUpdateTaskOpen] = useBooleanState();
  const projectId = useRouterQueryId();
  const taskId = useRouterQueryId('taskId');
  const { data: project } = useProjectDetailQuery({ projectId });
  const { data: task } = useTaskDetailQuery({ taskId });

  if (!project || !task) return null;

  const { minutes, hours } = getHoursFromMinute(task.spentTimes);
  const status = taskStatusesMapping[task.status];

  const userRole = user?.role;
  const isUpdateTask =
    userRole === UserRoles.ADMIN ||
    userRole === UserRoles.MANAGER ||
    task.assigned.id === user?.id;

  return (
    <>
      <StandartPageHeader
        title={`${task.name} (${project.name})`}
        caption={`Описание: ${task.description}`}
        action={
          isUpdateTask && (
            <Button
              sx={{
                m: 1
              }}
              onClick={() => toggleUpdateTaskOpen()}
              variant="contained"
              startIcon={<EditIcon fontSize="small" />}
            >
              Редактировать
            </Button>
          )
        }
      />

      <Grid
        sx={{ px: 4 }}
        container
        direction="row"
        justifyContent="center"
        alignItems="stretch"
        spacing={3}
      >
        <Grid item xs={12}>
          <Card>
            <BoxItemWrapper
              sx={{ p: 2 }}
              colorBefore={theme.colors[status.color].main}
            >
              <Typography
                variant="body2"
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                  mr: 2
                }}
              >
                <Tooltip arrow placement="top" title={status.tooltip}>
                  <DotLegend
                    style={{
                      background: `${theme.colors[status.color].main}`
                    }}
                  />
                </Tooltip>
                <span>
                  <Typography
                    component="span"
                    sx={{
                      fontWeight: 'bold'
                    }}
                  >
                    {task.name}
                  </Typography>
                </span>
              </Typography>
              <Typography
                variant="h6"
                sx={{
                  pl: 2.3,
                  py: 1
                }}
              >
                Ответственный:{' '}
                <b>
                  {task.assigned?.name} {task.assigned?.surname}
                </b>
              </Typography>
              <Typography
                variant="h6"
                sx={{
                  pl: 2.3,
                  py: 1
                }}
              >
                Время:{' '}
                <b>
                  {hours}:{minutes}
                </b>
              </Typography>
            </BoxItemWrapper>
          </Card>
        </Grid>
        <Grid item xs={12}>
          <TimeLogs />
        </Grid>
      </Grid>

      <Footer />

      <UpdateTaskPopup
        open={updateTaskOpen}
        toggleOpen={toggleUpdateTaskOpen}
        task={task}
        projectId={projectId}
      />
    </>
  );
}

const BoxItemWrapper = styled(Box)<{ colorBefore: string }>(
  ({ theme, colorBefore }) => `
    border-radius: ${theme.general.borderRadius};
    background: ${theme.colors.alpha.black[5]};
    position: relative;
    width: 100%;
    
    &::before {
      content: '.';
      background: ${colorBefore};
      color: ${colorBefore};
      border-radius: ${theme.general.borderRadius};
      position: absolute;
      text-align: center;
      width: 6px;
      left: 0;
      height: 100%;
      top: 0;
    }
    
    &.wrapper-info {
      &:before {
        background: ${theme.colors.info.main};
        color: ${theme.colors.info.main};
      }
    }
        
    &.wrapper-warning {
      &:before {
        background: ${theme.colors.warning.main};
        color: ${theme.colors.warning.main};
      }
    }
`
);

const DotLegend = styled('span')(
  ({ theme }) => `
    border-radius: 22px;
    width: ${theme.spacing(1.5)};
    height: ${theme.spacing(1.5)};
    display: inline-block;
    margin-right: ${theme.spacing(1)};
    margin-top: -${theme.spacing(0.1)};
`
);
