import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

export interface CreateChatRequest {
  personIds: number[];
  name: string;
}

export function useCreateChatMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, CreateChatRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, CreateChatRequest>(
    (data) => client.post('Chat/CreateChat', data),
    options
  );

  return mutation.mutateAsync;
}
