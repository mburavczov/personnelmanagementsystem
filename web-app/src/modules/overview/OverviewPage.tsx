import { Box, Button, Card, Container, styled } from '@mui/material';
import { Footer, Link, Logo } from '@/components';
import { Hero } from './Hero';
import { useAuth } from '@/hooks';

export function OverviewPage() {
  const { isAuthenticated } = useAuth();

  return (
    <OverviewWrapper>
      <HeaderWrapper>
        <Container maxWidth="lg">
          <Box display="flex" alignItems="center">
            <Logo />
            <Box
              display="flex"
              alignItems="center"
              justifyContent="space-between"
              flex={1}
            >
              <Box />
              <Box>
                <Button
                  component={Link}
                  href={isAuthenticated ? '/dashboard' : '/login'}
                  variant="contained"
                  sx={{ ml: 2 }}
                >
                  {isAuthenticated ? 'Домой' : 'Войти'}
                </Button>
              </Box>
            </Box>
          </Box>
        </Container>
      </HeaderWrapper>
      <Hero />
      <Footer />
    </OverviewWrapper>
  );
}

const OverviewWrapper = styled(Box)(
  ({ theme }) => `
    overflow: auto;
    background: ${theme.palette.common.white};
    flex: 1;
    overflow-x: hidden;
`
);

const HeaderWrapper = styled(Card)(
  ({ theme }) => `
  width: 100%;
  display: flex;
  align-items: center;
  height: ${theme.spacing(10)};
  margin-bottom: ${theme.spacing(10)};
`
);
