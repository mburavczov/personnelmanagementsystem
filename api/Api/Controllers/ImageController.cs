﻿using System.Threading.Tasks;
using Api.ApiModel.Response.Profile;
using Api.Repository;
using Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// Methods for images
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class ImageController : Controller
    {
        private readonly ImageRepository _imageRepository;
        private readonly ImageService _imageService;
    
        public ImageController(ImageRepository imageRepository, ImageService imageService)
        {
            _imageRepository = imageRepository;
            _imageService = imageService;
        }

        /// <summary>
        /// Upload image
        /// </summary>
        /// <response code="400">Zero or more than 1 files in request</response>
        [Authorize]
        [HttpPost("Upload")]
        [ProducesResponseType(typeof(UpdateAvatarResponse), 200)]
        public async Task<IActionResult> Upload()
        {
            if (Request.Form.Files.Count != 1)
            {
                return BadRequest();
            }

            string filePath;
            using (var fs = Request.Form.Files[0].OpenReadStream())
            {
                filePath = await _imageService.Upload(fs);
            }

            return Ok(new UpdateAvatarResponse
            {
                Link = filePath
            });
        }
    }
}