import Head from 'next/head';
import { Guest } from '@/components';
import { BaseLayout } from '@/layouts';
import { LoginPage } from '@/modules/auth';

function Login() {
  return (
    <>
      <Head>
        <title>Вход</title>
      </Head>
      <LoginPage />
    </>
  );
}

Login.getLayout = (page) => (
  <Guest>
    <BaseLayout>{page}</BaseLayout>
  </Guest>
);

export default Login;
