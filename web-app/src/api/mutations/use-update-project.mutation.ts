import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

export interface UpdateProjectRequest {
  projectId: number;
  name: string;
  description: string;
  managerId: number;
  customerId: number;
}

export function useUpdateProjectMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, UpdateProjectRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, UpdateProjectRequest>(
    (data) => client.post('Project/Update', data),
    options
  );

  return mutation.mutateAsync;
}
