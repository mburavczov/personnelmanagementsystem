import { useQuery } from 'react-query';
import { client } from '@/api';

export interface StatisticsResponse {
  items: {
    projectName: string;
    totalTasks: number;
    newTasks: number;
    inprogressTasks: number;
    waitTasks: number;
    doneTasks: number;
    spentTime: number;
  }[];
}

export function useStatisticsQuery() {
  return useQuery<StatisticsResponse>(
    ['statistics'],
    async () => {
      try {
        const dto = await client.get('Project/GetStat');
        return dto.data;
      } catch {
        console.log('fatal error');
      }
    },
    {}
  );
}
