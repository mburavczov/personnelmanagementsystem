import type { ReactElement } from 'react';
import { BaseLayout } from '@/layouts';
import Head from 'next/head';
import { OverviewPage } from '@/modules/overview';

function Overview() {
  return (
    <>
      <Head>
        <title>О проекте</title>
      </Head>
      <OverviewPage />
    </>
  );
}

Overview.getLayout = function getLayout(page: ReactElement) {
  return <BaseLayout>{page}</BaseLayout>;
};

export default Overview;
