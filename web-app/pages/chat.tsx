import Head from 'next/head';
import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';
import { ChatPage } from '@/modules/chat';

function ApplicationsMessenger() {
  return (
    <>
      <Head>
        <title>Чаты</title>
      </Head>
      <ChatPage />
    </>
  );
}

ApplicationsMessenger.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default ApplicationsMessenger;
