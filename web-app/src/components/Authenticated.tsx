import type { ReactNode } from 'react';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { useAuth } from 'src/hooks/useAuth';
import { useSnackbar } from 'notistack';
import { Slide } from '@mui/material';

interface AuthenticatedProps {
  children: ReactNode;
}

export function Authenticated({ children }: AuthenticatedProps) {
  const auth = useAuth();
  const router = useRouter();
  const [verified, setVerified] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    if (!router.isReady) {
      return;
    }

    if (!auth.isAuthenticated) {
      router.push({
        pathname: '/login',
        query: { backTo: router.asPath }
      });
    } else {
      setVerified(true);

      enqueueSnackbar('Вы успешно авторизовались!', {
        variant: 'success',
        anchorOrigin: {
          vertical: 'bottom',
          horizontal: 'right'
        },
        autoHideDuration: 2000,
        TransitionComponent: Slide
      });
    }
  }, [router.isReady]);

  if (!verified) {
    return null;
  }

  return <>{children}</>;
}
