import { TimeLog } from '@/models/timeLog';
import { User } from '@/models/user';
import { TaskStatuses } from '@/models/enums';

export interface Task {
  id: number;
  name: string;
  description: string;
  status: TaskStatuses;
  updatedAt: Date;
  spentTimes: number;
  assigned: User;
  timeLogs: TimeLog[];
}
