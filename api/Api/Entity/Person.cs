﻿using System.Collections.Generic;

namespace Api.Entity
{
    public class Person
    {
        public Person()
        {
            Chats = new List<Chat>();
            ChatMessages = new List<ChatMessage>();
            Projects = new List<Project>();
            ProjectTasks = new List<ProjectTask>();
            TimeLogs = new List<TimeLog>();
        }
        
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public PersonRole Role { get; set; }
        public PersonState State { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string About { get; set; }
        public string Position { get; set; }
        public string AvatarPath { get; set; }
        
        public virtual ICollection<Chat> Chats { get; set; }
        public virtual ICollection<ChatMessage> ChatMessages { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
        public virtual ICollection<ProjectTask> ProjectTasks { get; set; }
        public virtual ICollection<TimeLog> TimeLogs { get; set; }
    }
}