import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

export interface UpdateUserInfoRequest {
  personId: number;
  name: string;
  surname: string;
  phone?: string;
  position: string;
  about?: string;
  avatarPath?: string;
}

export function useUpdateUserInfoMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, UpdateUserInfoRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, UpdateUserInfoRequest>(
    (data) => client.post('Profile/UpdateInfo', data),
    options
  );

  return mutation.mutateAsync;
}
