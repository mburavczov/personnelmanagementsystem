import { Footer, Link, StandartPageHeader } from '@/components';
import { useProjectsQuery } from '@/api';
import {
  Box,
  Button,
  Card,
  CardHeader,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemSecondaryAction,
  ListSubheader,
  styled,
  Tooltip,
  Typography,
  useTheme
} from '@mui/material';
import React from 'react';
import { getHoursFromMinute, taskStatusesMapping } from '@/utils';

export function MyTasksPage() {
  const theme = useTheme();
  const { data } = useProjectsQuery({});
  const projects = data?.projects || [];

  return (
    <>
      <StandartPageHeader
        title="Мои задачи"
        caption="Здесь отображается список Ваших задач."
      />

      <Grid
        sx={{ px: 4 }}
        container
        direction="row"
        justifyContent="center"
        alignItems="stretch"
        spacing={3}
      >
        <Grid item xs={12}>
          <Card>
            <CardHeader
              sx={{
                display: { xs: 'block', sm: 'flex' }
              }}
              title="Список задач"
            />
            <List component="div" disablePadding>
              {projects
                .filter((project) => project.tasks.length !== 0)
                .map((project) => (
                  <>
                    <ListSubheader
                      component="div"
                      color="primary"
                      sx={{
                        background: `${theme.colors.alpha.white[100]}`,
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between'
                      }}
                    >
                      <Box>{project.name}</Box>
                    </ListSubheader>
                    <Divider />
                    {project.tasks.map((task) => {
                      const status = taskStatusesMapping[task.status];
                      const { minutes, hours } = getHoursFromMinute(
                        task.spentTimes
                      );

                      return (
                        <ListItem
                          component="div"
                          key={task.id}
                          sx={{
                            pt: 2,
                            pb: 0
                          }}
                        >
                          <BoxItemWrapper
                            sx={{ p: 2 }}
                            colorBefore={theme.colors[status.color].main}
                          >
                            <Typography
                              variant="body2"
                              sx={{
                                display: 'flex',
                                alignItems: 'center',
                                mr: 2
                              }}
                            >
                              <Tooltip
                                arrow
                                placement="top"
                                title={status.tooltip}
                              >
                                <DotLegend
                                  style={{
                                    background: `${
                                      theme.colors[status.color].main
                                    }`
                                  }}
                                />
                              </Tooltip>
                              <span>
                                <Typography
                                  component="span"
                                  sx={{
                                    fontWeight: 'bold'
                                  }}
                                >
                                  {task.name}
                                </Typography>
                              </span>
                            </Typography>
                            <Typography
                              variant="h6"
                              sx={{
                                pl: 2.3,
                                py: 1
                              }}
                            >
                              Ответственный:{' '}
                              <b>
                                {task.assigned?.name} {task.assigned?.surname}
                              </b>
                            </Typography>
                            <Typography
                              variant="h6"
                              sx={{
                                pl: 2.3,
                                py: 1
                              }}
                            >
                              Время:{' '}
                              <b>
                                {hours}:{minutes}
                              </b>
                            </Typography>
                          </BoxItemWrapper>
                          <ListItemSecondaryAction>
                            <Button
                              component={Link}
                              href={`/projects/${project.id}/tasks/${task.id}`}
                              sx={{
                                mr: 5
                              }}
                            >
                              Подробнее
                            </Button>
                          </ListItemSecondaryAction>
                        </ListItem>
                      );
                    })}
                    <Divider />
                  </>
                ))}
            </List>
          </Card>
        </Grid>
      </Grid>

      <Footer />
    </>
  );
}

const BoxItemWrapper = styled(Box)<{ colorBefore: string }>(
  ({ theme, colorBefore }) => `
    border-radius: ${theme.general.borderRadius};
    background: ${theme.colors.alpha.black[5]};
    position: relative;
    width: 100%;
    
    &::before {
      content: '.';
      background: ${colorBefore};
      color: ${colorBefore};
      border-radius: ${theme.general.borderRadius};
      position: absolute;
      text-align: center;
      width: 6px;
      left: 0;
      height: 100%;
      top: 0;
    }
    
    &.wrapper-info {
      &:before {
        background: ${theme.colors.info.main};
        color: ${theme.colors.info.main};
      }
    }
        
    &.wrapper-warning {
      &:before {
        background: ${theme.colors.warning.main};
        color: ${theme.colors.warning.main};
      }
    }
`
);

const DotLegend = styled('span')(
  ({ theme }) => `
    border-radius: 22px;
    width: ${theme.spacing(1.5)};
    height: ${theme.spacing(1.5)};
    display: inline-block;
    margin-right: ${theme.spacing(1)};
    margin-top: -${theme.spacing(0.1)};
`
);
