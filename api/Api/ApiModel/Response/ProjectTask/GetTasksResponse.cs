﻿using System.Collections.Generic;

namespace Api.ApiModel.Response.ProjectTask
{
    public class GetTasksResponse
    {
        public List<TaskItem> Tasks { get; set; }
    }
}