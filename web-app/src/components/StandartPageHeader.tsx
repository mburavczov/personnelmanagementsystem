import { ReactNode } from 'react';
import { Typography, Grid, Box } from '@mui/material';
import { PageTitleWrapper } from '@/components/PageTitleWrapper';

interface StandartPageHeaderProps {
  title: string;
  caption: string;
  action?: ReactNode;
  avatar?: ReactNode;
}

export function StandartPageHeader({
  title,
  caption,
  action,
  avatar
}: StandartPageHeaderProps) {
  return (
    <PageTitleWrapper>
      <Grid container justifyContent="space-between" alignItems="center">
        <Box display="flex" justifyContent="flex-start" alignItems="center">
          {avatar}
          <Grid item>
            <Typography variant="h3" component="h3" gutterBottom>
              {title}
            </Typography>
            <Typography variant="subtitle2">{caption}</Typography>
          </Grid>
        </Box>
        <Grid item>{action}</Grid>
      </Grid>
    </PageTitleWrapper>
  );
}
