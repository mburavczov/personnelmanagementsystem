export function getToken() {
  if (typeof window === 'undefined') return null;
  const accessToken = window.localStorage.getItem('accessToken');

  return `Bearer ${accessToken}`;
}
