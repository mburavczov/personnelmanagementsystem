import axios from 'axios';
import { getToken } from '@/utils';

const { NEXT_PUBLIC_API_BASE_URL } = process.env;

export const client = axios.create({
  baseURL: `${NEXT_PUBLIC_API_BASE_URL}/api/`,
  headers: { Authorization: getToken() }
});
