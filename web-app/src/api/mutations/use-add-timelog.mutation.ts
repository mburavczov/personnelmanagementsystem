import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

export interface AddTimelogRequest {
  taskId: number;
  personId: number;
  minutes: number;
  comment: string;
}

export function useAddTimelogMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, AddTimelogRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, AddTimelogRequest>(
    (data) => client.post('ProjectTask/AddTimeLog', data),
    options
  );

  return mutation.mutateAsync;
}
