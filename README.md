# Personnel Management System

Система управления проектами и сотрудниками. Позволяет создавать пользователей 
4 ролей (админ, менеджер, заказчик, сотрудник), редактировать профиль и загружать
аватар, создавать проекты и задачи, добавлять людей в проекты, назначать исполнителей 
на задачи, отмечать затраченное на задачи время, составлять отчёты по проектам и 
сотрудникам. Так же есть реализованы чаты, как групповые, так и One-to-One

**Кейс** - Информационная система "Личный кабинет сотрудника” (кейс 7)


## Стек технологий

Бэкенд написан на [ASP.NET Core 5](https://dotnet.microsoft.com/apps/aspnet).
Фронтенд на [React](https://reactjs.org/) с использованием фреймворков
[Next.js](https://nextjs.org/) и библиотеки 
[React Query](https://react-query.tanstack.com/). Для загрузки картинок используется
CDN [Selectel](https://selectel.ru/)


Настроен CI/CD, API и web-сервис запускаются внутри [Docker](https://www.docker.com/)
контейнера и разворачиваются на [Heroku](https://www.heroku.com/).


## Демо

API: https://phoenix-lk-api.herokuapp.com/swagger/index.html  
WEB: https://phoenix-lk-web.herokuapp.com/

Доступы (логин:пароль):
- string@string.ru:string (Admin)
- string10@string.ru:string (Manager)


## Развёртывание

Сервисы запускаются внутри докер контейнеров.
Команды нужно выпонять в корневой директории проекта.

Запуск API
```bash
docker build -f api/Dockerfile -t lk_api .
docker run -p 8080:8080 -e "CDN_TOKEN=<TOKEN>" -e "PORT=8080" -it lk_api
```
Проверить запуск можно открыв swagger http://localhost:8080/swagger/index.html

Запуск web приложения
```bash
docker build -f web-app/Dockerfile -t lk_web .
docker run -p 8081:8081 -e "NEXTAUTH_URL='localhost:8081'" -e "PORT=8081" -it lk_web
```
Приложение будет доступно по адресу http://localhost:8081/
