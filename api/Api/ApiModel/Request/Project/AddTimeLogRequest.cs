﻿namespace Api.ApiModel.Request.Project
{
    public class AddTimeLogRequest
    {
        public int TaskId { get; set; }
        public int PersonId { get; set; }
        public int Minutes { get; set; }
        public string Comment { get; set; }
    }
}