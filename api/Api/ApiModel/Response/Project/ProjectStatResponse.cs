﻿using System.Collections.Generic;

namespace Api.ApiModel.Response.Project
{
    public class ProjectStatResponse
    {
        public List<ProjectStatItem> Items { get; set; }
    }

    public class ProjectStatItem
    {
        public string ProjectName { get; set; }
        public int TotalTasks { get; set; }
        public int NewTasks { get; set; }
        public int InprogressTasks { get; set; }
        public int WaitTasks { get; set; }
        public int DoneTasks { get; set; }
        public int SpentTime { get; set; }
    }
}