import Head from 'next/head';
import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';
import { ProjectsTasksPage } from '@/modules/projects';

function ProjectsTasks() {
  return (
    <>
      <Head>
        <title>Задачи проекта</title>
      </Head>
      <ProjectsTasksPage />
    </>
  );
}

ProjectsTasks.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default ProjectsTasks;
