export interface ChatPersons {
  personId: number;
  name: string;
}

export interface Chat {
  id: number;
  persons: ChatPersons[];
}
