export function getHoursFromMinute(minutes: number) {
  const min = minutes % 60;
  return {
    hours: Math.floor(minutes / 60),
    minutes: min > 10 ? min : `0${min}`
  };
}
