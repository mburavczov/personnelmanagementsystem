﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Api.Entity.Configuration
{
    public class TimeLogConfiguration : IEntityTypeConfiguration<TimeLog>
    {
        public void Configure(EntityTypeBuilder<TimeLog> builder)
        {
            builder.HasKey(x => x.Id);
            
            builder.Property(x => x.TaskId).IsRequired();
            builder.Property(x => x.PersonId).IsRequired();
            builder.Property(x => x.Minutes).IsRequired();

            builder.HasOne(x => x.Person)
                .WithMany(x => x.TimeLogs)
                .HasForeignKey(x => x.PersonId);

            builder.HasOne(x => x.ProjectTask)
                .WithMany(x => x.TimeLogs)
                .HasForeignKey(x => x.TaskId);
        }
    }
}