import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

export interface CreateProjectRequest {
  name: string;
  description: string;
  managerId: number;
  customerId: number;
}

export function useCreateProjectMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, CreateProjectRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, CreateProjectRequest>(
    (data) => client.post('Project/Create', data),
    options
  );

  return mutation.mutateAsync;
}
