import { Footer, StandartPageHeader } from '@/components';
import React from 'react';
import { useAuth, useBooleanState } from '@/hooks';
import { CreateUserPopup } from '@/modules/users/CreateUserPopup';
import { UserRoles } from '@/models/enums';
import { Button, Grid } from '@mui/material';
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import { UsersList } from '@/modules/users/UsersList';

export function UsersPage() {
  const { user } = useAuth();
  const [createUserOpen, toggleCreateUserOpen] = useBooleanState();
  const isCreateUserAction = user?.role === UserRoles.ADMIN;

  return (
    <>
      <StandartPageHeader
        title="Пользователи"
        caption="Здесь Вы можете посмотреть пользователей системы"
        action={
          isCreateUserAction && (
            <Button
              sx={{
                m: 1
              }}
              onClick={() => toggleCreateUserOpen()}
              variant="contained"
              startIcon={<AddTwoToneIcon fontSize="small" />}
            >
              Добавить пользователя
            </Button>
          )
        }
      />

      <Grid
        sx={{ px: 4 }}
        container
        direction="row"
        justifyContent="center"
        alignItems="stretch"
        spacing={3}
      >
        <Grid item xs={12}>
          <UsersList />
        </Grid>
      </Grid>

      <Footer />

      {isCreateUserAction && (
        <CreateUserPopup
          open={createUserOpen}
          toggleOpen={toggleCreateUserOpen}
        />
      )}
    </>
  );
}
