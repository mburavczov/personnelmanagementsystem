﻿namespace Api.ApiModel.Request.Project
{
    public class AddMemberRequest
    {
        public int PersonId { get; set; }
        public int ProjectId { get; set; }
    }
}