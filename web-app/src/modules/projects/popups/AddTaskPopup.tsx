import { DialogPopup } from '@/components';
import {
  Autocomplete,
  Button,
  CircularProgress,
  TextField,
  Typography
} from '@mui/material';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useAddTaskToProjectMutation, usePersonsQuery } from '@/api';
import { UserRoles } from '@/models/enums';
import { useQueryClient } from 'react-query';

interface AddTaskPopupProps {
  open: boolean;
  toggleOpen: () => void;
  projectId: number;
}

export function AddTaskPopup({
  open,
  toggleOpen,
  projectId
}: AddTaskPopupProps) {
  const queryClient = useQueryClient();
  const { data: users = [] } = usePersonsQuery({
    role: UserRoles.EMPLOYEE
  });
  const addTask = useAddTaskToProjectMutation({
    onSuccess: () => {
      toggleOpen();
      queryClient.invalidateQueries(['project', projectId]);
    }
  });
  const formik = useFormik({
    initialValues: {
      projectId,
      taskName: '',
      taskDescription: '',
      assignedId: ''
    },
    validationSchema: Yup.object({
      projectId: Yup.number().required('Выберите проект.'),
      taskName: Yup.string().required('Введите название задачи.'),
      taskDescription: Yup.string().required('Введите описание задачи.'),
      assignedId: Yup.number().required('Выберите ответственного.')
    }),
    onSubmit: (values): void => {
      addTask(values);
    }
  });

  return (
    <>
      <DialogPopup
        header={
          <Typography variant="h4" gutterBottom>
            Добавление задачи в проект
          </Typography>
        }
        actions={
          <>
            <Button color="secondary" onClick={toggleOpen}>
              Отмена
            </Button>
            <Button
              onClick={() => formik.handleSubmit()}
              type="submit"
              startIcon={
                formik.isSubmitting ? <CircularProgress size="1rem" /> : null
              }
              disabled={formik.isSubmitting}
              variant="contained"
            >
              Добавить
            </Button>
          </>
        }
        open={open}
        onClose={toggleOpen}
      >
        <TextField
          error={Boolean(formik.touched.taskName && formik.errors.taskName)}
          fullWidth
          margin="normal"
          helperText={formik.touched.taskName && formik.errors.taskName}
          label={'Название задачи'}
          name="taskName"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.taskName}
          variant="outlined"
        />
        <TextField
          error={Boolean(
            formik.touched.taskDescription && formik.errors.taskDescription
          )}
          fullWidth
          margin="normal"
          helperText={
            formik.touched.taskDescription && formik.errors.taskDescription
          }
          label={'Описание задачи'}
          name="taskDescription"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.taskDescription}
          variant="outlined"
          multiline
        />
        <Autocomplete
          options={users.map((user) => ({
            label: `${user.name} ${user.surname}`,
            value: user.id
          }))}
          onChange={(_, selectedOption) => {
            formik.setFieldValue('assignedId', selectedOption?.value as number);
          }}
          renderInput={(params) => (
            <TextField
              fullWidth
              margin="normal"
              {...params}
              label={'Ответственный'}
              error={Boolean(
                formik.touched.assignedId && formik.errors.assignedId
              )}
              helperText={formik.touched.assignedId && formik.errors.assignedId}
            />
          )}
        />
      </DialogPopup>
    </>
  );
}
